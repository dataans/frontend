FROM node:latest as build

COPY . /app
WORKDIR /app
RUN mv env .env

RUN yarn
RUN yarn build

FROM nginx:alpine
#!/bin/sh

COPY nginx.conf /etc/nginx/nginx.conf

## Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

COPY --from=build /app/build /usr/share/nginx/html

EXPOSE 8080

ENTRYPOINT ["nginx", "-g", "daemon off;"]
