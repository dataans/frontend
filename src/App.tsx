import React from 'react';
import Routing from './containers/Routing';

import 'antd/dist/antd.min.css';

function App() {
  return (
    <Routing />
  );
}

export default App;
