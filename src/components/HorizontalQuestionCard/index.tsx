import React from 'react';
import { DownCircleOutlined, UpCircleFilled, CommentOutlined, EyeOutlined } from '@ant-design/icons';
import { Tooltip } from 'antd';

import Tag, { TagProps } from '../Tag';
import styles from './style.module.sass';
import { Link } from 'react-router-dom';
import { Question } from '../../types/question';

export interface HorizontalQuestionCardProps {
  question: Question,
}

const HorizontalQuestionCard: React.FC<HorizontalQuestionCardProps> = ({ question }) => {
  return (
    <div className={styles.card} >
      <div className={styles.cardVotes}>
        <UpCircleFilled className={styles.iconChecked} />
        <span>{question.upVotes}</span>
        <DownCircleOutlined className={styles.iconUnchecked} />
      </div>
      <div className={styles.cardContent}>
        <Link className={styles.title} to={{ pathname: `/question/${question.id}` }}>{question.title}</Link>
        <div>{question.text}</div>
        <div className={styles.bottomPanel}>
          <div className={styles.cardStatics}>
            {/* <Tooltip title="Answers"><CommentOutlined /> {answers}</Tooltip> */}
            <Tooltip title="Views"><EyeOutlined /> {question.views}</Tooltip>
            {question.tags.map(e => <Tag id={e.id} name={e.name} key={e.id} />)}
          </div>
          <div>
            <span>Asked at {question.createdAt.toLocaleString()} by </span>
            <Link className={styles.authorName} to={{ pathname: `/user/${question.creator.id}` }}>
              <span>{question.creator.fullName}</span>
              <img src={question.creator.avatarUrl} />
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HorizontalQuestionCard;