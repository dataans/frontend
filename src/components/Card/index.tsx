import React from 'react';
import Tag from '../Tag';

import styles from './style.module.sass';

export interface CardProps {
  id: string;
  title: string;
  description: string;
  imgUrl: string;
}

const Card: React.FC<CardProps> = ({ id, title, description, imgUrl }) => {
  return (
    <div className={styles.card}>
      <div className={styles.cardDescription}>
        <Tag id={id} name={title} />
        <span>{description} items</span>
      </div>
    </div>
  );
};

export default Card;