import React from 'react';
import { Link } from 'react-router-dom';

import styles from './styles.module.sass';

export interface TagProps {
  id: string;
  name: string;
}

const Tag: React.FC<TagProps> = ({ id, name }) => {
  return (
    <Link className={styles.tag} to={{ pathname: `/tag/${id}` }}>#{name}</Link>
  );
};

export default Tag;