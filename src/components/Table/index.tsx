import React from 'react';

import styles from './styles.module.sass';

export interface TableProps {
  data: any[],
}

const row = (rowData: any) => {
  const cells = new Array();
  for (const cell of rowData) {
    cells.push(<span className={styles.cell}>{cell.value}</span>);
  }

  return (
    <div className={styles.row}>
      {cells}
    </div>
  );
};

const titleRow = (rowData: any) => {
  const cells = new Array();
  for (const cell of rowData) {
    cells.push(<span className={`${styles.cell} ${styles.titleCell}`}>{cell.name}</span>);
  }

  return (
    <div className={styles.row}>
      {cells}
    </div>
  );
};

const tables = (tablesData: any) => tablesData.map((tableData: any) =>
  <div className={styles.tableContainer}>
    <span className={styles.tableName}>{tableData.name}</span>
    <div>
      {titleRow(tableData.data[0])}
      {tableData.data.map((r: any) => row(r))}
    </div>
  </div>
);

const Table: React.FC<TableProps> = ({ data }) => {
  return (
    <div className={styles.tables}>
      {tables(data)}
    </div>
  );
};

export default Table;