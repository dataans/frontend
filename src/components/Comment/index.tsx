import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import {
  MenuOutlined,
  EditOutlined,
  PlusOutlined,
  UpOutlined,
  CaretUpOutlined,
  DeleteOutlined,
} from '@ant-design/icons';
import { Menu, Dropdown } from 'antd';

import { useUserStore } from '../../helpers/userStore';
import { Comment as CommentData, CommentFull, emptyFullComment, CommentType, emptyComment } from '../../types/comment';
import CommentEditor from './CommentEditor';
import { formatDateTime } from '../../helpers/timeDate';
import { User } from '../../types/user';

import styles from './styles.module.sass';

const menu = (comment: CommentFull, user: User, onEdit: () => void, onDelete: () => void) => (
  <Menu className={styles.menu}>
    {comment.creator.id === user.id &&
      <Menu.Item key="1" className={`${styles.menuItem} ${styles.blueMenuItem}`} onClick={onEdit}>
        <div className={styles.menuItemGroup}>
          <EditOutlined />
          <span>Edit</span>
        </div>
      </Menu.Item>
    }
    {comment.creator.id === user.id &&
      <Menu.Item key="2" className={`${styles.menuItem} ${styles.redMenuItem}`} onClick={onDelete}>
        <div className={styles.menuItemGroup}>
          <DeleteOutlined />
          <span>Delete</span>
        </div>
      </Menu.Item>
    }
    {comment.creator.id === user.id &&
      <Menu.Divider className={styles.menuDivider} />
    }
    <Menu.Item key="3" className={`${styles.menuItem} ${styles.blueMenuItem}`}>
      <div className={styles.menuItemGroup}>
        <PlusOutlined />
        <span>{formatDateTime(comment.createdAt)}</span>
      </div>
    </Menu.Item>
    <Menu.Item key="4" className={`${styles.menuItem} ${styles.blueMenuItem}`}>
      <div className={styles.menuItemGroup}>
        <EditOutlined />
        <span>{formatDateTime(comment.updatedAt)}</span>
      </div>
    </Menu.Item>
  </Menu>
);

export interface CommentProps {
  comment: CommentFull;
  onSubmit: (data: CommentData) => void;
  onVote: (commentId: string) => void;
  onDelete: (commentId: string) => void;
}

const Comment: React.FC<CommentProps> = ({ comment, onSubmit, onVote, onDelete }) => {
  let [edit, setEdit] = useState<boolean>(false);

  const user = useUserStore(state => state.user);

  return (
    <div className={styles.horizontal}>
      {
        (comment.id && edit) &&
        <CommentEditor cancel={() => setEdit(false)} comment={comment} ok={(id, text) => {
          onSubmit({
            id,
            text,
            creatorId: comment.creator.id,
            createdAt: comment.createdAt,
            updatedAt: new Date(),
            upVotes: 0,
            isVoted: false,
          });
          setEdit(false);
        }} />
      }
      {
        (comment.id && !edit) &&
        <div className={styles.comment}>
          <div className={styles.labelWithIcon}>
            <span>{comment.upVotes}</span>
            {comment.isVoted ?
              <CaretUpOutlined className={styles.icon} onClick={() => onVote(comment.id)}/>
            :
              <UpOutlined className={styles.icon} onClick={() => onVote(comment.id)}/>
            }
          </div>
          <div className={styles.commentContent}>
            <span>{comment.text}</span>
            <div className={styles.itemsList}>
              <Link to={{ pathname: `/user/${comment.creator.id}` }} className={styles.authorName}>
                <img src={comment.creator.avatarUrl} />
                <span>{comment.creator.fullName}</span>
              </Link>
              <Dropdown overlay={menu(comment, user, () => setEdit(true), () => onDelete(comment.id))}>
                <MenuOutlined />
              </Dropdown>
            </div>
          </div>
        </div>
      }
      {
        (!comment.id && edit) &&
        <CommentEditor cancel={() => setEdit(false)} comment={emptyFullComment(user)} ok={(_id, text) => {
          onSubmit({
            text,
            id: '',
            creatorId: '',
            createdAt: new Date(),
            updatedAt: new Date(),
            upVotes: 0,
            isVoted: false,
          })
          setEdit(false);
        }} />
      }
      {
        (!comment.id && !edit) &&
        <span className={styles.clickableLabel} onClick={() => setEdit(true)}>Add new comment</span>
      }
    </div>
  );
};

export default Comment;