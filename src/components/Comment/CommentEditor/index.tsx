import React, { useState } from 'react';

import { Button } from 'antd';

import { CommentFull } from '../../../types/comment';

import styles from './styles.module.sass';

export interface CommentEditorProps {
  comment: CommentFull;
  ok: (id: string, text: string) => void;
  cancel: () => void;
}

const CommentEditor: React.FC<CommentEditorProps> = ({ cancel, comment, ok }) => {
  const [commentText, setCommentText] = useState<string>(comment.text);

  return (
    <div className={styles.editor}>
      <textarea value={commentText} rows={2} onChange={event => setCommentText(event.target.value)} />
      <div className={styles.horizontal}>
        <Button type="link" onClick={cancel} size="small" danger>Cancel</Button>
        <Button type="primary" onClick={() => ok(comment.id, commentText)} size="small">Ok</Button>
      </div>
    </div>
  );
};

export default CommentEditor;