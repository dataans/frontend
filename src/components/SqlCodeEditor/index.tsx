import React, { useState, useEffect } from 'react';
import CodeEditor from '@uiw/react-textarea-code-editor';
import {
  CaretRightOutlined,
  SaveOutlined,
  AppstoreAddOutlined,
  EyeOutlined,
  EyeInvisibleOutlined,
  SettingOutlined,
  DatabaseOutlined
} from '@ant-design/icons';
import { Dropdown, Menu, Tooltip, message } from 'antd';

import Table from '../Table';
import { Sample } from "../../types/sample";

import styles from './styles.module.sass';

export interface CodeEditorProps {
  sample: Sample;
  edit: boolean;
  setQueryCode: (code: string) => void;
  setSchemaCode: (code: string) => void;
};

const databasesMenu = (
  <Menu>
    <Menu.Item key="0">
      <span>PostgreSQL</span>
    </Menu.Item>
    <Menu.Item key="0">
      <span>MySQL</span>
    </Menu.Item>
    <Menu.Item key="0">
      <span>MSSQL</span>
    </Menu.Item>
  </Menu>
);

const SqlCodeEditor: React.FC<CodeEditorProps> = ({ sample, setQueryCode, setSchemaCode, edit }) => {
  const [showData, setShowData] = useState<boolean>(false);
  const [data, setData] = useState<any[]>([]);

  const [ws, setWs] = useState<WebSocket | null>(null);

  useEffect(() => {
    const w = new WebSocket('wss://api.dataans.com/api/v1/ws');
    w.addEventListener('open', () => {
      console.log('ws open');
    });
    w.addEventListener('message', m => {
      const data = m.data;
      console.log('new message from ws: ');
      console.log(data);
      
      try {
        const wsMessage = JSON.parse(m.data);
        if (wsMessage.error) {
          message.error(wsMessage.message + ': ' + wsMessage.error, 15);
        } else {
          if (wsMessage.message) {
            message.success(wsMessage.message);
          }
          if (wsMessage.data) {
            console.log(wsMessage.data);
            setData(JSON.parse(wsMessage.data));
          }
        }
      } catch (_err) {
        message.success(m.data);
      }
    })
    w.addEventListener('close', () => {
      console.log('ws closed');
    })
    w.addEventListener('error', err => {
      console.log('ws error: ' + err);
    });

    setWs(w);
  }, []);

  const executeQuery = () => {
    if (ws !== null) {
      ws.send(JSON.stringify({ query: sample.query, id: sample.id }));
    }
  };

  const executeSchema = () => {
    if (ws !== null) {
      ws.send(JSON.stringify({ schema: sample.schema, id: sample.id }));
    }
  };

  return (
    <div className={styles.sqlEditor}>
      <div className={styles.verticalSmall}>
        <CodeEditor
          value={sample.query}
          language="sql"
          placeholder="-- Please enter SQL code."
          padding={15}
          onChange={event => setQueryCode(event.target.value)}
          style={{ width: "100%", height: "100%", fontFamily: "'JetBrains Mono', monospace" }}
          disabled={!edit}
        />
        <div className={styles.toolBar}>
          <Tooltip title="Generate schema from queries">
            <SettingOutlined className={styles.toolBarIcon} style={{ color: "#cfc8c2" }} />
          </Tooltip>
          <Tooltip title="Download code">
            <SaveOutlined className={styles.toolBarIcon} style={{ color: "#cfc8c2" }} />
          </Tooltip>
            {showData ?
              <Tooltip title="Hide queried data">
                <EyeInvisibleOutlined className={styles.toolBarIcon} style={{ color: "#cfc8c2" }} onClick={() => setShowData(!showData)} />
              </Tooltip>
            :
              <Tooltip title="Show queried data">
                <EyeOutlined className={styles.toolBarIcon} style={{ color: "#cfc8c2" }} onClick={() => setShowData(!showData)} />
              </Tooltip>
            }
          <Tooltip title="Run sql code">
            <CaretRightOutlined
              className={styles.toolBarIcon}
              style={{ color: "#cfc8c2" }}
              onClick={executeQuery}
            />
          </Tooltip>
        </div>
      </div>
      <div className={styles.verticalSmall}>
        <CodeEditor
          value={sample.schema}
          language="sql"
          placeholder="-- Please enter SQL code."
          padding={15}
          onChange={event => setSchemaCode(event.target.value)}
          style={{ width: "100%", height: "100%", fontFamily: "'JetBrains Mono', monospace" }}
          disabled={!edit}
        />
        <div className={styles.toolBar}>
          <Tooltip title="Select database type">
            <Dropdown overlay={databasesMenu} trigger={['click']} className={styles.toolBarIcon}>
              <div className={styles.databaseDropDownTitle}>
                <DatabaseOutlined />
                <span>Select database</span>
              </div>
            </Dropdown>
          </Tooltip>
          <Tooltip title="Download schema code">
            <SaveOutlined className={styles.toolBarIcon} style={{ color: "#cfc8c2" }} />
          </Tooltip>
          <Tooltip title="Update schema">
            <AppstoreAddOutlined className={styles.toolBarIcon} style={{ color: "#cfc8c2" }} />
          </Tooltip>
          <Tooltip title="Alter schema">
            <CaretRightOutlined
              className={styles.toolBarIcon}
              style={{ color: "#cfc8c2" }}
              onClick={executeSchema}
            />
          </Tooltip>
        </div>
      </div>
      <div className={styles.dataContainer}>
        {showData && <Table data={data}/>}
      </div>
    </div>
  );
};

export default SqlCodeEditor;