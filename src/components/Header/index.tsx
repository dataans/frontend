import React from 'react';
import HeaderLogo from './HeaderLogo';
import HeaderTools from './HeaderTools';
import HeaderSearch from './HeaderSearch';
import { User } from '../../types/user';

import styles from './styles.module.sass';

export interface HeaderProps {
  user: User;
}

const Header: React.FC<HeaderProps> = ({ user }) => {
  return (
    <header className={styles.headerContainer}>
      <HeaderLogo />
      <HeaderSearch />
      <HeaderTools user={user} />
    </header>
  );
};

export default Header;