import React from 'react';
import { Space, Menu, Dropdown } from 'antd';
import { Link } from 'react-router-dom';
import {
  PlusCircleOutlined,
  QuestionCircleOutlined,
  FileTextOutlined,
  PartitionOutlined,
} from '@ant-design/icons';

import styles from './styles.module.sass';

const menu = (
  <Menu className={styles.menu}>
    <Menu.Item key="0" className={styles.menuItem}>
      <Link to={{ pathname: '/question/new' }}><QuestionCircleOutlined /> Question</Link>
    </Menu.Item>
    <Menu.Item key="1" className={styles.menuItem} disabled>
      <Link to={{ pathname: '/article/new' }}><FileTextOutlined /> Article</Link>
    </Menu.Item>
    <Menu.Item key="3" className={styles.menuItem} disabled>
      <Link to={{ pathname: '/course/new' }}><PartitionOutlined /> Course</Link>
    </Menu.Item>
  </Menu>
);

const HeaderLogo: React.FC = () => {
  return (
    <Space direction="horizontal" size="small" className={styles.toolsContainer}>
      <Link to={{ pathname: '/' }}>
        <span className={styles.appName}>Dataans</span>
      </Link>
      <Dropdown overlay={menu}>
        <PlusCircleOutlined className={styles.toolIcon} /> 
      </Dropdown>
    </Space>
  );
};

export default HeaderLogo;