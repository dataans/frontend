import React from 'react';
import { Space, Menu, Dropdown, Button } from 'antd';
import { Link } from 'react-router-dom';
import { NotificationOutlined, CommentOutlined, SettingOutlined, DashboardOutlined, LogoutOutlined } from '@ant-design/icons';

import { User } from '../../../types/user';

import styles from './styles.module.sass';

export interface HeaderToolsProps {
  user: User;
}

const menu = (
  <Menu className={styles.menu}>
    <Menu.Item key="0" className={styles.menuItem}>
      <Link to={{ pathname: '/user/account' }}><DashboardOutlined /> Profile</Link>
    </Menu.Item>
    <Menu.Item key="1" className={styles.menuItem}>
      <Link to={{ pathname: '/user/security' }}><SettingOutlined /> Settings</Link>
    </Menu.Item>
    <Menu.Divider className={styles.menuDivider} />
    <Menu.Item key="3" className={styles.menuItem}>
      <Link to={{ pathname: '/sign-in' }}><LogoutOutlined /> Sign out</Link>
    </Menu.Item>
  </Menu>
);

const HeaderTools: React.FC<HeaderToolsProps> = ({ user }) => {
  return (
    <Space direction="horizontal" size="small">
      <NotificationOutlined className={styles.toolIcon} />
      <CommentOutlined className={styles.toolIcon} />
      {user.id ?
        <Dropdown overlay={menu} className={styles.profileButton}>
          <div className={styles.toolsContainer}>
            <span>{user.username}</span>
            <img src={user.avatarUrl} alt="avatar" className={styles.avatar} />
          </div>
        </Dropdown>
      :
        <Link to={{ pathname: '/sign-in' }}>
          <Button type="primary" size="middle">
            <span>Sign in/up</span>
          </Button>
        </Link>
      }
    </Space>
  );
};

export default HeaderTools;