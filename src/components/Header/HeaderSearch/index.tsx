import React from 'react';
import { Input } from 'antd';

import styles from './styles.module.sass';

const HeaderSearch: React.FC = () => {
  return (
    <div className={styles.searchContainer}>
      <Input.Search placeholder="Search..." style={{ border: "none" }} className={styles.searchInput} disabled />
    </div>
  );
};

export default HeaderSearch;