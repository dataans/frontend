import { Button, Input } from 'antd';
import React, { useState } from 'react';

import styles from './styles.module.sass';

export interface AskPasswordProps {
  onCancel: () => void;
  onSubmit: (password: string) => void;
}

const AskPassword: React.FC<AskPasswordProps> = ({ onCancel, onSubmit }) => {
  const [password, setPassword] = useState<string>('');

  return (
    <div className={styles.page}>
      <div className={styles.form}>
        <span className={styles.title}>Confirm access</span>
        <span className={styles.label}>Password</span>
        <Input allowClear size="middle" className={styles.input} type="password" onChange={event => setPassword(event.target.value)} />
        <span className={styles.forgotPassword}>Forgot password?</span>
        <div className={styles.horizontal}>
          <Button type="ghost" size="middle" onClick={onCancel} style={{ color: "#cfc8c2" }}>
            <span>Cancel</span>
          </Button>
          <Button type="primary" size="middle" onClick={() => onSubmit(password)}>
            <span>Confirm password</span>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default AskPassword;