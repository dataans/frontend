import React, { useEffect } from 'react';
import { GoogleLogin } from 'react-google-login';
import { Button, message } from 'antd';
import { GithubOutlined, GoogleOutlined } from '@ant-design/icons';
import apiClient from '../../helpers/api';
import { useNavigate } from 'react-router-dom';

import styles from './styles.module.sass';

export interface GitHubCredentials {
  clientId: string;
  redirectUri: string;
}

export interface GoogleCredentials {
  clientId: string;
}

export interface OAuthProps {
  github: GitHubCredentials;
  google: GoogleCredentials;
  setLoading: (loading: boolean) => void;
}

const successGoogleOAuth = (response: any, setLoading: any, navigate: any) => {
  setLoading(true);
  apiClient.post({ endpoint: '/auth/google', body: { code: response.tokenId } }).then(() => {
    navigate('/');
  }).catch(async err => {
    message.error(err + '');
  }).finally(() => setLoading(false));
};

const failureGoogleOAuth = async (response: any) => {
  message.error(response + '');
};


const OAuth: React.FC<OAuthProps> = ({ github, google, setLoading }) => {
  const navigate = useNavigate();

  useEffect(() => {
    document.title = 'Sign in - DataAns'

    const url = window.location.href;
    const hasCode = url.includes("?code=");

    if (hasCode) {
      const code = url.split("?code=")[1];

      setLoading(true);
      apiClient.post({ endpoint: '/auth/github', body: { code } }).then(res => {
        navigate('/');
      }).catch(async err => {
        message.error(err + '');
      }).finally(() => setLoading(false));
    }
  }, []);

  return (
    <div className={styles.oauthSitesContainer}>
      <Button type="default" htmlType="button" size="large">
        <a href={`https://github.com/login/oauth/authorize?scope=user&client_id=${github.clientId}&redirect_uri=${github.redirectUri}`}>
          <GithubOutlined className="site-form-item-icon" /> GitHub
        </a>
      </Button>
    </div>
  );
};

export default OAuth;