import { message } from 'antd';
import { createComment, loadAnswer, updateAnswer, vote } from '../../helpers/api/answer';
import { deleteComment, updateComment, voteComment } from '../../helpers/api/comment';
import { createSample, deleteSample, updateSample } from '../../helpers/api/sample';
import { AnswerFull, VoteType, Answer } from '../../types/answer';
import { commentFull2Comment, CommentType, Comment } from '../../types/comment';
import { emptySample, Sample } from '../../types/sample';

export const publishComment = async (answerId: string, commentData: Comment, setAnswer: (a: AnswerFull) => void) => {
  try {
    let _comment = await createComment({
      commentType: CommentType.ANSWER,
      parentId: answerId,
      text: commentData.text,
    });

    setAnswer(await loadAnswer(answerId));

    message.success('Comment added successfully');
  } catch (e) {
    message.error(e + '');
  }
};

export const updateCommentCallback = async (answerId: string, commentData: Comment, setAnswer: (a: AnswerFull) => void) => {
  try {
    let _comment = await updateComment({
      id: commentData.id,
      text: commentData.text,
    });

    setAnswer(await loadAnswer(answerId));

    message.success('Comment updated successfully');
  } catch (e) {
    message.error(e + '');
  }
};

export const voteAnswerComment = async (answerId: string, commentId: string, setAnswer: (a: AnswerFull) => void) => {
  try {
    let _comment = await voteComment(commentId);

    setAnswer(await loadAnswer(answerId));

    message.success('Vote saved!');
  } catch (e) {
    message.error(e + '');
  }
};

export const deleteAnswerComment = async (answerId: string, commentId: string, setAnswer: (a: AnswerFull) => void) => {
  try {
    await deleteComment(commentId);

    setAnswer(await loadAnswer(answerId));
  } catch (e) {
    message.error(e + '');
  }
};

export const voteAnswer = async (answerId: string, voteType: VoteType, setAnswer: (a: AnswerFull) => void) => {
  try {
    const answer = await vote({ answerId, voteType });

    setAnswer(answer);

    message.success('Vote saved!');
  } catch (e) {
    message.error(e + '');
  }
};

export const createAnswerSample = async (sample: Sample, answerData: AnswerFull, setSample: (s: Sample) => void, setAnswer: (a: Answer) => void) => {
  const { query, schema } = sample;
  message.loading('Please wait...');
  try {
    const sample = await createSample({ query, schema });
    if (answerData.id) {
      // we do not need parentId (questionId) here
      const answer = await updateAnswer({ id: answerData.id, text: answerData.text, sampleId: sample.id, parentId: '' });

      setAnswer({
        id: answer.id,
        text: answer.text,
        createdAt: answer.createdAt,
        updatedAt: answer.updatedAt,
        creatorId: answer.creator.id,
        comments: answer.comments.map(commentFull2Comment),
        votes: answer.votes,
        isVoted: answer.isVoted,
        sample,
      });
    }
    setSample(sample);
  } catch (e) {
    message.error(e + '');
  }
};

export const deleteAnswerSample = async (sample: Sample, answerData: AnswerFull, setSample: (s: Sample) => void, setAnswer: (a: Answer) => void) => {
  message.loading('Please wait...');
  try {
    // we do not need parentId (questionId) here
    const answer = answerData.id
      ? await updateAnswer({ id: answerData.id, text: answerData.text, sampleId: undefined, parentId: '' })
      : answerData;

    await deleteSample(sample.id);
    const newSample = emptySample();

    if (answerData.id) {
      setAnswer({
        id: answer.id,
        text: answer.text,
        createdAt: answer.createdAt,
        updatedAt: answer.updatedAt,
        creatorId: answer.creator.id,
        comments: answer.comments.map(commentFull2Comment),
        votes: answer.votes,
        isVoted: answer.isVoted,
        sample: newSample,
      });
    }
    setSample(newSample);
  } catch (e) {
    message.error(e + '');
  }
};

export const saveSample = async (sample: Sample, setSample: (s: Sample) => void): Promise<void> => {
  try {
    const updatedSample = await updateSample(sample);
    setSample(updatedSample);

    message.success('Code sample updated!');
  } catch (e) {
    message.error(e + '');
  }
}
