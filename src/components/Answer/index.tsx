import React, { useState, useEffect } from 'react';
import { Button, Divider, Dropdown, Menu, message, Switch, Tooltip } from 'antd';
import MDEditor from '@uiw/react-md-editor';
import { Link } from 'react-router-dom';
import {
  UpOutlined,
  DownOutlined,
  CaretUpOutlined,
  CaretDownOutlined,
  ConsoleSqlOutlined,
  DeleteOutlined,
  EditOutlined,
  MenuOutlined,
  SaveOutlined,
  CloseCircleOutlined,
} from '@ant-design/icons';

import { Answer as AnswerData, AnswerFull, VoteType } from '../../types/answer';
import { emptySample, Sample } from '../../types/sample';
import SqlCodeEditor from '../SqlCodeEditor';
import Comment from '../Comment';
import { commentFull2Comment, emptyFullComment } from '../../types/comment';
import { useUserStore } from '../../helpers/userStore';
import { formatDateTime } from '../../helpers/timeDate';
import { loadSample } from '../../helpers/api/sample';
import {
  createAnswerSample,
  deleteAnswerComment,
  deleteAnswerSample,
  publishComment,
  saveSample,
  updateCommentCallback,
  voteAnswer,
  voteAnswerComment,
} from './actions';

import styles from './styles.module.sass';

const onSwitchChange = async (checked: boolean, sample: Sample, answerData: AnswerFull, setSample: (s: Sample) => void, setAnswer: (a: AnswerData) => void) => {
  if (checked) {
    await createAnswerSample(sample, answerData, setSample, setAnswer);
  } else {
    await deleteAnswerSample(sample, answerData, setSample, setAnswer);
  }
};

const menu = (onEdit: () => void, onDelete: () => void) => (
  <Menu className={styles.menu}>
    <Menu.Item key="1" className={`${styles.menuItem} ${styles.blueMenuItem}`} onClick={onEdit}>
      <div className={styles.menuItemGroup}>
        <EditOutlined />
        <span>Edit</span>
      </div>
    </Menu.Item>
    <Menu.Item key="2" className={`${styles.menuItem} ${styles.redMenuItem}`} onClick={onDelete} disabled>
      <div className={styles.menuItemGroup}>
        <DeleteOutlined />
        <span>Delete</span>
      </div>
    </Menu.Item>
  </Menu>
);

export interface AnswerProps {
  answer: AnswerFull;
  // when answer was created or updated
  onSubmit: (answerData: AnswerData) => Promise<void>;
  // when new comment, update comment, upvote, etc
  onAnswerChange: (answer: AnswerFull) => void;
}

const Answer: React.FC<AnswerProps> = ({ answer, onSubmit, onAnswerChange }) => {
  const user = useUserStore(state => state.user);

  const [loading, setLoading] = useState<boolean>(false);
  const [text, setText] = useState<string>(answer.text);
  const [sample, setSample] = useState<Sample>(emptySample());
  const [isSampleEdited, setIsSampleEdited] = useState<boolean>(false);
  const [edit, setEdit] = useState<boolean>(answer.id === '');

  useEffect(() => {
    if (answer.sampleId) {
      loadSample(answer.sampleId)
        .then(sample => setSample(sample))
        .catch(e => message.error(e + ''));
    }
  }, [answer]);

  return (
    <div className={styles.vertical}>
      <Divider className={styles.horizontalDivider} />
      {answer.id &&
        <div className={styles.answerDetails}>
          <div className={styles.labelWithIcon}>
            <span>{answer.votes}</span>
            {answer.isVoted == VoteType.UP ?
              <CaretUpOutlined className={styles.icon} onClick={() => voteAnswer(answer.id, VoteType.UP, onAnswerChange)}/>
            :
              <UpOutlined className={styles.icon} onClick={() => voteAnswer(answer.id, VoteType.UP, onAnswerChange)}/>
            }
            {answer.isVoted == VoteType.DOWN ?
              <CaretDownOutlined className={styles.icon} onClick={() => voteAnswer(answer.id, VoteType.DOWN, onAnswerChange)}/>
            :
              <DownOutlined className={styles.icon} onClick={() => voteAnswer(answer.id, VoteType.DOWN, onAnswerChange)}/>
            }
          </div>
          <Link to={{ pathname: `/user/${answer.creator.id}` }} className={styles.author}>
            <img src={answer.creator.avatarUrl} />
            <span>{answer.creator.fullName}</span>
          </Link>
          <span>answered: {formatDateTime(answer.createdAt)}</span>
          <span>updated: {formatDateTime(answer.updatedAt)}</span>
          {edit && answer.creator.id === user.id &&
            <Tooltip title="Save changes">
              <SaveOutlined
                onClick={() => {
                  onSubmit({
                    id: answer.id,
                    text,
                    createdAt: answer.createdAt,
                    updatedAt: answer.updatedAt,
                    creatorId: answer.creator.id,
                    comments: answer.comments.map(commentFull2Comment),
                    votes: answer.votes,
                    isVoted: answer.isVoted,
                    sample: sample,
                  });
                  setEdit(false);
                }}
                className={styles.toolBarIcon}
              />
            </Tooltip>
          }
          {edit && answer.creator.id === user.id &&
            <Tooltip title="Cancel changes">
              <CloseCircleOutlined
                onClick={() => {
                  setText(answer.text);
                  setEdit(false);
                }}
                className={styles.toolBarIcon}
              />
            </Tooltip>
          }
          <div className={styles.menuSpace} />
          {answer.creator.id === user.id &&
            <Dropdown overlay={menu(() => setEdit(true), () => {})}>
              <MenuOutlined />
            </Dropdown>
          }
        </div>
      }
      <MDEditor
        value={text}
        onChange={event => setText(event || '')}
        preview={edit ? "live" : "preview"}
        hideToolbar={!edit}
      />
      {((sample.creatorId === user.id && user.id) || (!sample.id && !answer.id) || (answer.creator.id === user.id)) &&
        <div className={styles.answerOptions}>
          <ConsoleSqlOutlined style={{ "fontSize": "0.75em" }} />
          <Tooltip title={sample.id ? "Remove SQL code sample" : "Add SQL code sample"}>
            <Switch
              style={{ width: "fit-content", backgroundColor: "#24233b" }}
              checked={sample.id !== ''}
              onChange={(checked, _) => onSwitchChange(checked, sample, answer, setSample, onSubmit)}
            />
          </Tooltip>
          {isSampleEdited &&
            <Tooltip title="Save code sample without execution">
              <SaveOutlined className={styles.answerOptionsIcon} onClick={() => {
                saveSample(sample, setSample);
                setIsSampleEdited(false);
              }}/>
            </Tooltip>
          }
        </div>
      }
      {sample.id &&
        <SqlCodeEditor
          edit={edit || sample.creatorId === user.id}
          sample={sample}
          setQueryCode={query => {
            setSample({ ...sample, query });
            setIsSampleEdited(true);
          }}
          setSchemaCode={schema => {
            setSample({ ...sample, schema });
            setIsSampleEdited(true);
          }}
        />
      }
      {answer.id &&
        <div className={styles.comments}>
          {answer.comments.sort((c1, c2) => c1.createdAt.getTime() - c2.createdAt.getTime()).map(comment => 
            <Comment
              key={comment.id}
              comment={comment}
              onSubmit={data => updateCommentCallback(answer.id, data, onAnswerChange)}
              onVote={commentId => voteAnswerComment(answer.id, commentId, onAnswerChange)}
              onDelete={commentId => deleteAnswerComment(answer.id, commentId, onAnswerChange)}
            />
          )}
          <Comment
            comment={emptyFullComment(user)}
            onSubmit={data => publishComment(answer.id, data, onAnswerChange)}
            onVote={commentId => voteAnswerComment(answer.id, commentId, onAnswerChange)}
            onDelete={commentId => deleteAnswerComment(answer.id, commentId, onAnswerChange)}
          />
        </div>
      }
      <div className={styles.horizontal}>
        {edit &&
          <div className={styles.horizontal}>
            <Button type="primary" size="small" danger ghost>
              <span>Discard</span>
            </Button>
            <Button type="primary" size="small" onClick={() => {
                onSubmit({
                  id: answer.id,
                  text,
                  createdAt: answer.createdAt,
                  updatedAt: answer.updatedAt,
                  creatorId: answer.creator.id,
                  comments: [],
                  votes: 0,
                  isVoted: undefined,
                  sample: sample.id ? sample : undefined,
                }).then(() => {
                  setText('');
                  setSample(emptySample());
                  setIsSampleEdited(false);
                });
              }
            } loading={loading}>
              <span>Publish</span>
            </Button>
          </div>
        }
      </div>
    </div>
  );
};

export default Answer;