import { emptyUser, User } from './user';

export interface Tag {
  id: string;
  name: string;
  createdAt: Date;
  creatorId: string;
}

export interface TagInfo {
  id: string;
  name: string;
  createdAt: Date;
  creator: User;
  questionsAmount: number;
}

export const emptyTag = (): Tag => ({
  id: '',
  name: '',
  createdAt: new Date(),
  creatorId: '',
});

export const emptyTagInfo = (): TagInfo => ({
  id: '',
  name: '',
  createdAt: new Date(),
  creator: emptyUser(),
  questionsAmount: 0,
});
