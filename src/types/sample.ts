export interface SampleCreate {
  query: string;
  schema: string;
}

export interface Sample {
  id: string;
  query: string;
  schema: string;
  lastExecution: Date;
  creatorId: string;
}

export const emptySample = (): Sample => ({
  id: '',
  query: '-- place your query here',
  schema: '-- place your schema here',
  lastExecution: new Date(),
  creatorId: '',
});

export const sampleToCreateSample = (data: Sample): SampleCreate => ({
  query: data.query,
  schema: data.schema,
});
