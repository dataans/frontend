import { CommentFull } from './comment';
import { Tag } from './tag';
import { User } from './user';

export interface QuestionCreate {
  title: string;
  text: string;
  sampleId: string | undefined;
  tags: string[];
}

export interface QuestionUpdate {
  id: number;
  title: string;
  text: string;
  sampleId: string | undefined;
  tags: string[];
}

export interface Question {
  id: number;
  title: string;
  text: string;
  createdAt: Date;
  updatedAt: Date;
  creator: User;
  upVotes: number;
  savedTimes: number;
  views: number;
  sampleId: string | undefined;
  tags: Tag[];
  comments: CommentFull[],

  isVoted: boolean;
  isSaved: boolean;
}

export interface SimpleQuestion {
  id: number;
  title: string;
  text: string;
  createdAt: Date;
  updatedAt: Date;
  creatorId: string;
  views: number;
  sampleId: string | undefined;
}

export const emptyQuestion = (creator: User): Question => ({
  id: 0,
  title: 'Title',
  text: '**Describe your problem here**',
  createdAt: new Date(),
  updatedAt: new Date(),
  creator,
  upVotes: 0,
  savedTimes: 0,
  views: 0,
  sampleId: '',
  tags: [],
  comments: [],

  isVoted: false,
  isSaved: false,
});

export const questionToQuestionCreate = (data: Question): QuestionCreate => ({
  title: data.title,
  text: data.text,
  sampleId: data.sampleId,
  tags: data.tags.map(tag => tag.id),
});
