export interface SignInCredentials {
  login: string;
  password: string;
}

export interface User {
  id: string;
  username: string;
  fullName: string;
  avatarUrl: string;
}

export interface UserFull {
  id: string;
  username: string;
  primaryEmail: string;
  emails: string[];
  fullName: string;
  avatarUrl: string;
  is2faEnabled: boolean;
}

export interface UpdateUserAccountDataRequest {
  fullName: string;
  username: string;
}

export const emptyUser = (): User => ({
  id: '',
  username: '',
  fullName: '',
  avatarUrl: '',
});

export const emptyFullUser = (): UserFull => ({
  id: '',
  username: '',
  fullName: '',
  primaryEmail: '',
  emails: [],
  avatarUrl: '',
  is2faEnabled: false,
});

export interface SignInResponse {
  needs2fa: boolean;
}

export interface TotpRequest {
  code: string;
}

export interface EmailWithPassword {
  email: string;
  password: string;
}

export interface Enable2Fa {
  enable:boolean;
  password: string;
}

export type AddEmailRequest = EmailWithPassword;

export type DeleteEmailRequest = EmailWithPassword;

export type SetPrimaryEmailRequest = EmailWithPassword;

export interface ChangePasswordRequest {
  password: string;
  newPassword: string;
}

export interface RecoveryCodeRequest {
  password: string;
}

export interface RecoveryCodeResponse {
  secret: string;
  codes: string[];
}

export const emptyRecoveryCodes = (): RecoveryCodeResponse => ({
  secret: '',
  codes: [],
});
