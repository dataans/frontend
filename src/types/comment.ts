import { User } from './user';

export enum CommentType {
  QUESTION,
  ANSWER,
}

export interface CommentCreate {
  commentType: CommentType;
  // id of the entity to which comment is attached
  parentId: string;
  text: string;
}

export interface CommentUpdate {
  id: string,
  text: string,
}

export interface Comment {
  id: string;
  text: string;
  creatorId: string;
  createdAt: Date;
  updatedAt: Date;
  upVotes: number;
  isVoted: boolean;
}

export interface CommentFull {
  id: string;
  text: string;
  creator: User;
  createdAt: Date;
  updatedAt: Date;
  upVotes: number;
  isVoted: boolean;
}

export const emptyComment = (): Comment => ({
  id: '',
  text: '',
  creatorId: '',
  createdAt: new Date(),
  updatedAt: new Date(),
  upVotes: 0,
  isVoted: false,
});

export const emptyFullComment = (creator: User): CommentFull => ({
  id: '',
  text: '',
  creator,
  createdAt: new Date(),
  updatedAt: new Date(),
  upVotes: 0,
  isVoted: false,
});

export const commentFull2Comment = (comment: CommentFull): Comment => {
  const { id, creator, createdAt, updatedAt, upVotes, isVoted, text, } = comment;
  return {
    id,
    text,
    creatorId: creator.id,
    createdAt,
    updatedAt,
    upVotes,
    isVoted,
  };
};
