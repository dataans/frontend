import { Comment, CommentFull } from './comment';
import { emptySample, Sample } from './sample';
import { emptyUser, User } from './user';

export interface AnswerCreate {
  text: string;
  sample: Sample | undefined;
  parentId: number;
}

export interface AnswerUpdate {
  id: string;
  text: string;
  sampleId: string | undefined;
  parentId: string;
}

export enum VoteType {
  UP = "Up",
  DOWN = "Down",
}

export interface VoteCreate {
  answerId: string;
  voteType: VoteType;
}

export interface Answer {
  id: string;
  text: string;
  createdAt: Date;
  updatedAt: Date;
  creatorId: string;
  comments: Comment[];
  votes: number;
  isVoted: VoteType | undefined,
  sample: Sample | undefined;
}

export interface AnswerFull {
  id: string;
  text: string;
  createdAt: Date;
  updatedAt: Date;
  creator: User;
  comments: CommentFull[];
  votes: number;
  isVoted: VoteType | undefined,
  sampleId: string | undefined;
}

export interface SimpleAnswer {
  id: string;
  text: string;
  createdAt: Date;
  updatedAt: Date;
  creatorId: string;
  votes: number;
  isVoted: VoteType | undefined,
  sampleId: string | undefined;
}

export const emptyAnswer = (): Answer => ({
  id: '',
  text: '',
  createdAt: new Date(),
  updatedAt: new Date(),
  creatorId: '',
  comments: [],
  votes: 0,
  isVoted: undefined,
  sample: emptySample(),
});

export const emptyFullAnswer = (creator: User | undefined): AnswerFull => ({
  id: '',
  text: '',
  createdAt: new Date(),
  updatedAt: new Date(),
  creator: creator || emptyUser(),
  comments: [],
  votes: 0,
  isVoted: undefined,
  sampleId: undefined,
});
