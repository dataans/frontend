import create from 'zustand';
import { message } from 'antd';

import { emptyFullUser, UserFull } from '../types/user';
import apiClient from './api';

export const loadUserData = async (): Promise<UserFull> => {
  try {
    let response = await apiClient.get({ endpoint: '/user' });
    return response.json();
  } catch (e) {
    message.error(e + '');
    throw e;
  }
};

export const useUserStore = create((set: any) => ({
  user: emptyFullUser(),
  setUser: (user: UserFull) => set({ user }),
}));
