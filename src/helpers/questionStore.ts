import create from 'zustand';

import { AnswerFull } from '../types/answer';
import { emptyQuestion, Question } from '../types/question';
import { emptySample, Sample } from "../types/sample";
import { Tag } from '../types/tag';
import { emptyUser } from '../types/user';

export const useQuestionStore = create((set: any) => ({
  question: emptyQuestion(emptyUser()),
  sample: emptySample(),
  tags: [],
  answers: [],

  setQuestion: (question: Question) => set({ question }),
  setQuestionTitle: (title: string) => set((state: any) => {
    const question = { ...state.question };
    question.title = title;
    return { question };
  }),
  setQuestionText: (text: string) => set((state: any) => {
    const question = { ...state.question };
    question.text = text;
    return { question };
  }),
  setQuestionTags: (tags: Tag[]) => set((state: any) => {
    const question = { ...state.question };
    question.tags = tags;
    return { question };
  }),
  setSample: (sample: Sample) => set({ sample }),
  setQueryCode: (query: string) => set((state: any) => {
    const sample = { ...state.sample };
    sample.query = query;
    return { sample };
  }),
  setSchemaCode: (schema: string) => set((state: any) => {
    const sample = { ...state.sample };
    sample.schema = schema;
    return { sample };
  }),
  setTags: (tags: Tag[]) => set((_state: any) => {
    return { tags };
  }),
  setAnswers: (answers: AnswerFull[]) => set((_state: any) => {
    return { answers };
  }),
  setAnswer: (updatedAnswer: AnswerFull) => set((state: any) => {
    const answers = state.answers.map((answer: any) => answer.id === updatedAnswer.id ? { ...updatedAnswer } : { ...answer });
    return { answers };
  }),
}));
