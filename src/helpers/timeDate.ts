export const formatDateTime = (d: Date): string => {
  return d.toLocaleString();
};
