import apiClient from '.';
import { Question } from '../../types/question';
import { TagInfo } from '../../types/tag';
import { transformQuestion } from './question';

export const loadTagInfo = async (id: string): Promise<TagInfo> => {
  const response = await apiClient.get({ endpoint: `/tag/${id}/info` });
  const tag = await response.json() as TagInfo;
  
  tag.createdAt = new Date(tag.createdAt);

  return tag;
}

export const getTagQuestions = async (id: string): Promise<Question[]> => {
  const response = await apiClient.get({ endpoint: `/tag/${id}/question` });
  const questions = await response.json() as Question[];
  
  questions.map(q => transformQuestion(q));

  return questions;
}
