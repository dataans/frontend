import apiClient from '.';
import { Comment, CommentFull, CommentUpdate } from '../../types/comment';

export const voteComment = async (commentId: string): Promise<CommentFull> => {
  const response = await apiClient.post({ endpoint: `/comment/${commentId}/vote` });
  const comment = await response.json() as CommentFull;

  comment.createdAt = new Date(comment.createdAt);
  comment.updatedAt = new Date(comment.updatedAt);

  return comment;
};

export const updateComment = async (body: CommentUpdate): Promise<CommentFull> => {
  const response = await apiClient.put({ endpoint: `/comment`, body });
  const comment = await response.json() as CommentFull;

  comment.createdAt = new Date(comment.createdAt);
  comment.updatedAt = new Date(comment.updatedAt);

  return comment;
}

export const deleteComment = async (commentId: string): Promise<void> => {
  const _response = await apiClient.delete({ endpoint: `/comment/${commentId}` });
}

const transformComment = (comment: Comment) => {
  comment.createdAt = new Date(comment.createdAt);
  comment.updatedAt = new Date(comment.updatedAt);
}

export const loadVotedComments = async (): Promise<Comment[]> => {
  const response = await apiClient.get({ endpoint: '/comment/user/voted' });
  const comments = await response.json() as Comment[];

  comments.map(comment => transformComment(comment));

  return comments;
}

export const loadCreatedComments = async (user_id: string): Promise<Comment[]> => {
  const response = await apiClient.get({ endpoint: `/comment/created/${user_id}` });
  const comments = await response.json() as Comment[];

  comments.map(comment => transformComment(comment));

  return comments;
}
