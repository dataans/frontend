import apiClient from '.';
import { AnswerCreate, AnswerFull, AnswerUpdate, SimpleAnswer, VoteCreate, VoteType } from '../../types/answer';
import { CommentCreate, CommentFull } from '../../types/comment';

const parseVoteType = (voteType: string | undefined): VoteType | undefined => {
  if (!voteType) {
    return undefined;
  }
  if (voteType === "Up") {
    return VoteType.UP;
  }
  if (voteType === "Down") {
    return VoteType.DOWN;
  }
  return undefined;
}

export const createComment = async (data: CommentCreate): Promise<CommentFull> => {
  const response = await apiClient.post({
    endpoint: `/answer/${data.parentId}/comment`,
    body: {
      text: data.text,
    }
  });
  const comment = await response.json() as CommentFull;

  comment.createdAt = new Date(comment.createdAt);
  comment.updatedAt = new Date(comment.updatedAt);

  return comment;
}

export const loadAnswer = async (answerId: string): Promise<AnswerFull> => {
  const response = await apiClient.get({ endpoint: `/answer/${answerId}` });

  const answer = await response.json() as AnswerFull;

  answer.createdAt = new Date(answer.createdAt);
  answer.updatedAt = new Date(answer.updatedAt);
  answer.isVoted = parseVoteType(answer.isVoted + '');

  answer.comments.map(comment => {
    comment.createdAt = new Date(comment.createdAt);
    comment.updatedAt = new Date(comment.updatedAt);
  });

  return answer;
};

export const vote = async (data: VoteCreate): Promise<AnswerFull> => {
  const _response = await apiClient.post({ endpoint: `/answer/${data.answerId}/vote`, body: { voteType: data.voteType } });

  return loadAnswer(data.answerId);
};

export const createAnswer = async (data: AnswerCreate): Promise<AnswerFull> => {
  const response = await apiClient.post({
    endpoint: `/question/${data.parentId}/answer`, body: {
      text: data.text,
      sampleId: data.sample?.id,
    }
  });

  const answer = await response.json() as AnswerFull;

  answer.createdAt = new Date(answer.createdAt);
  answer.updatedAt = new Date(answer.updatedAt);
  answer.isVoted = parseVoteType(answer.isVoted + '');

  answer.comments.map(comment => {
    comment.createdAt = new Date(comment.createdAt);
    comment.updatedAt = new Date(comment.updatedAt);
  });

  return answer;
};

export const updateAnswer = async (data: AnswerUpdate): Promise<AnswerFull> => {
  const response = await apiClient.put({
    endpoint: `/answer`,
    body: {
      id: data.id,
      text: data.text,
      sampleId: data.sampleId,
    }
  });

  const answer = await response.json() as AnswerFull;

  answer.createdAt = new Date(answer.createdAt);
  answer.updatedAt = new Date(answer.updatedAt);
  answer.isVoted = parseVoteType(answer.isVoted + '');

  answer.comments.map(comment => {
    comment.createdAt = new Date(comment.createdAt);
    comment.updatedAt = new Date(comment.updatedAt);
  });

  return answer;
};

export const loadAnswers = async (id: number): Promise<AnswerFull[]> => {
  const response = await apiClient.get({ endpoint: `/question/${id}/answer` });
  const answers = await response.json() as AnswerFull[];

  answers.map(answer => {
    answer.createdAt = new Date(answer.createdAt);
    answer.updatedAt = new Date(answer.updatedAt);
    answer.comments.map(comment => {
      comment.createdAt = new Date(comment.createdAt);
      comment.updatedAt = new Date(comment.updatedAt);
    });
    answer.isVoted = parseVoteType(answer.isVoted + '');
  });

  return answers;
};

const transformSimpleAnswer = (answer: SimpleAnswer) => {
  answer.createdAt = new Date(answer.createdAt);
  answer.updatedAt = new Date(answer.updatedAt);
}

export const loadVotedAnswers = async (): Promise<SimpleAnswer[]> => {
  const response = await apiClient.get({ endpoint: '/answer/user/voted' });
  const answers = await response.json() as SimpleAnswer[];

  answers.map(answer => transformSimpleAnswer(answer));

  return answers;
}

export const loadCreatedAnswers = async (user_id: string): Promise<SimpleAnswer[]> => {
  const response = await apiClient.get({ endpoint: `/answer/created/${user_id}` });
  const answers = await response.json() as SimpleAnswer[];

  answers.map(answer => transformSimpleAnswer(answer));

  return answers;
}
