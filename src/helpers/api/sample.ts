import apiClient from '.';
import { SampleCreate, Sample } from '../../types/sample';

export const createSample = async (body: SampleCreate): Promise<Sample> => {
  const response = await apiClient.post({ endpoint: '/sample', body });
  const sample = await response.json() as Sample;
  
  sample.lastExecution = new Date(sample.lastExecution);

  return sample;
};

export const updateSample = async (data: Sample): Promise<Sample> => {
  const response = await apiClient.put({ endpoint: '/sample', body: {
      id: data.id,
      query: data.query,
      schema: data.schema,
    }
  });
  const sample = await response.json() as Sample;
  
  sample.lastExecution = new Date(sample.lastExecution);

  return sample;
}

export const deleteSample = async (sampleId: string): Promise<void> => {
  const _response = await apiClient.delete({ endpoint: `/sample/${sampleId}` });
};

export const loadSample = async (id: string): Promise<Sample> => {
  const response = await apiClient.get({ endpoint: `/sample/${id}` });
  const sample = await response.json() as Sample;
  
  sample.lastExecution = new Date(sample.lastExecution);

  return sample;
};