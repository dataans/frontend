import apiClient from '.';
import {
  AddEmailRequest,
  ChangePasswordRequest,
  DeleteEmailRequest,
  Enable2Fa,
  RecoveryCodeRequest,
  RecoveryCodeResponse,
  SetPrimaryEmailRequest,
  TotpRequest,
  UpdateUserAccountDataRequest,
  UserFull,
} from '../../types/user';

export const updateAccountData = async (body: UpdateUserAccountDataRequest): Promise<UserFull> => {
  const response = await apiClient.put({ endpoint: '/user', body });
  const user = await response.json() as UserFull;
  
  return user;
}

export const addEmail = async (body: AddEmailRequest): Promise<UserFull> => {
  const response = await apiClient.post({ endpoint: '/auth/email', body });
  const user = await response.json() as UserFull;
  
  return user;
};

export const deleteEmail = async (body: DeleteEmailRequest): Promise<UserFull> => {
  const response = await apiClient.delete({ endpoint: '/auth/email', body });
  const user = await response.json() as UserFull;
  
  return user;
};

export const setPrimaryEmail = async (body: SetPrimaryEmailRequest): Promise<UserFull> => {
  const response = await apiClient.put({ endpoint: '/auth/email', body });
  const user = await response.json() as UserFull;
  
  return user;
};

export const changePassword = async (body: ChangePasswordRequest): Promise<void> => {
  const _response = await apiClient.put({ endpoint: '/auth/password', body });
};

export const submitTotp = async (body: TotpRequest): Promise<void> => {
  const _response = await apiClient.post({ endpoint: '/auth/2fa/totp', body });
};

export const enable2Fa = async (body: Enable2Fa): Promise<UserFull> => {
  const response = await apiClient.post({ endpoint: '/auth/2fa', body });
  const user = await response.json() as UserFull;
  
  return user;
};

export const regenerateRecoveryCodes = async (body: RecoveryCodeRequest): Promise<RecoveryCodeResponse> => {
  const response = await apiClient.post({ endpoint: '/auth/2fa/recovery_code', body });
  const codes = await response.json() as RecoveryCodeResponse;
  
  return codes;
};
