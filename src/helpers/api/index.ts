export interface IRequestArgs {
  method?: string;
  endpoint: string;
  credentials?: string;
  contentType?: string;
  withoutExplicitContentType?: boolean;
  args?: { [key: string]: string };
  body?: object;
}

function formatArgs(args: { [key: string]: string }): string {
  let res = '';
  for (const key in args) {
    res += `${key}=${args[key]}`;
  }
  return res;
}

function getFetchUrl(args: IRequestArgs) {
  return `${process.env.REACT_APP_API}/api/v1` + args.endpoint + (args.args ? `?${formatArgs(args.args)}` : '');
}

function getFetchArgs(args: IRequestArgs): RequestInit {
  const headers: any = {};
  if (args.contentType) {
    headers['Content-Type'] = args.contentType;
  } else {
    if (!args.withoutExplicitContentType) {
      headers['Content-Type'] = 'application/json';
    }
  }
  console.dir(headers);
  return {
    method: args.method,
    headers,
    credentials: args.credentials as RequestCredentials,
    body: JSON.stringify(args.body)
  };
}

export async function throwIfResponseFailed(res: Response) {
  // 200 - OK
  // 201 - CREATED
  // 410 - Gone (DELETED)
  if (res.status !== 200 && res.status !== 201 && res.status !== 204 && res.status !== 410) {
    throw await res.text();
  }
}

export async function callWebApi(args: IRequestArgs) {
  args.credentials = 'include';
  const res = await fetch(getFetchUrl(args), getFetchArgs(args));
  await throwIfResponseFailed(res);
  return res;
}

const apiClient = {
  get: (args: IRequestArgs) => callWebApi({ method: 'GET', ...args }),
  post: (args: IRequestArgs) => callWebApi({ method: 'POST', ...args }),
  put: (args: IRequestArgs) => callWebApi({ method: 'PUT', ...args }),
  delete: (args: IRequestArgs) => callWebApi({ method: 'DELETE', ...args })
};

export default apiClient;