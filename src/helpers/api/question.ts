import apiClient from '.';
import { CommentCreate, CommentFull } from '../../types/comment';
import { Question, QuestionCreate, QuestionUpdate, SimpleQuestion } from '../../types/question';
import { Tag } from '../../types/tag';

export const transformQuestion = (question: Question) => {
  question.createdAt = new Date(question.createdAt);
  question.updatedAt = new Date(question.updatedAt);

  question.comments.map(comment => {
    comment.createdAt = new Date(comment.createdAt);
    comment.updatedAt = new Date(comment.updatedAt);
  });
}

export const createQuestion = async (body: QuestionCreate): Promise<Question> => {
  const response = await apiClient.post({ endpoint: '/question', body });
  const question = await response.json() as Question;
  
  transformQuestion(question);

  return question;
};

export const updateQuestion = async (body: QuestionUpdate): Promise<Question> => {
  const response = await apiClient.put({ endpoint: '/question', body });
  const question = await response.json() as Question;
  
  transformQuestion(question);

  return question;
};

export const loadQuestion = async (id: number): Promise<Question> => {
  const response = await apiClient.get({ endpoint: `/question/${id}` });
  const question = await response.json() as Question;

  transformQuestion(question);

  return question;
};

export const vote = async (id: number): Promise<Question> => {
  const _response = await apiClient.post({ endpoint: `/question/${id}/vote` });
  return loadQuestion(id);
}

export const addToSaved = async (id: number): Promise<Question> => {
  const _response = await apiClient.post({ endpoint: `/question/${id}/save` });
  return loadQuestion(id);
}

export const createComment = async (data: CommentCreate): Promise<CommentFull> => {
  const response = await apiClient.post({
    endpoint: `/question/${data.parentId}/comment`,
    body: {
      text: data.text,
    }
  });
  const comment = await response.json() as CommentFull;

  comment.createdAt = new Date(comment.createdAt);
  comment.updatedAt = new Date(comment.updatedAt);

  return comment;
}

export const loadTags = async (): Promise<Tag[]> => {
  const response = await apiClient.get({ endpoint: '/tag' });
  const tags = await response.json() as Tag[];

  return tags.map(tag => ({
    id: tag.id,
    name: tag.name,
    createdAt: new Date(tag.createdAt),
    creatorId: tag.creatorId,
  } as Tag));
};

const transformSimpleQuestion = (question: SimpleQuestion) => {
  question.createdAt = new Date(question.createdAt);
  question.updatedAt = new Date(question.updatedAt);
}

export const loadSavedQuestions = async (): Promise<SimpleQuestion[]> => {
  const response = await apiClient.get({ endpoint: '/question/user/saved' });
  const questions = await response.json() as SimpleQuestion[];

  questions.map(question => transformSimpleQuestion(question));

  return questions;
}

export const loadVotedQuestions = async (): Promise<SimpleQuestion[]> => {
  const response = await apiClient.get({ endpoint: '/question/user/voted' });
  const questions = await response.json() as SimpleQuestion[];

  questions.map(question => transformSimpleQuestion(question));

  return questions;
}

export const loadCreatedQuestions = async (user_id: string): Promise<SimpleQuestion[]> => {
  const response = await apiClient.get({ endpoint: `/question/created/${user_id}` });
  const questions = await response.json() as SimpleQuestion[];

  questions.map(question => transformSimpleQuestion(question));

  return questions;
}
