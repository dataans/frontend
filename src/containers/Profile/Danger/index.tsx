import { DeleteOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import React, { useEffect } from 'react';

import { User } from '../../../types/user';

import styles from './styles.module.sass';

export interface DangerProps {
  user: User;
}

const Danger: React.FC<DangerProps> = ({ user }) => {

  useEffect(() => {
    document.title = 'Danger settings - Dataans';
  }, []);

  return (
    <div className={styles.dangerPage}>
      <div className={styles.optionContainer}>
        <div className={styles.optionDescription}>
          <span>Delete account</span>
          <span>Delete permanently account (questions, answers, comments will be saved but under the [deleted account])</span>
        </div>
        <div className={styles.optionValue}>
          <Button type="primary" danger style={{ "marginLeft": "0.5em" }} size="large" icon={<DeleteOutlined />} disabled>
            <span>Delete account</span>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Danger;