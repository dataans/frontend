import { Spin } from 'antd';
import React, { useState, useEffect } from 'react';

import { SimpleAnswer as Answer } from '../../../../types/answer';
import SimpleQuestion from './SimpleAnswer'; 

import styles from './styles.module.sass';

export interface SimpleANswersListProps {
  loadAnswers: () => Promise<Answer[]>;
}

const SimpleAnswersList: React.FC<SimpleANswersListProps> = ({ loadAnswers }) => {
  const [answers, setAnswers] = useState<Answer[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    setLoading(true);
    loadAnswers()
      .then(answers => setAnswers(answers))
      .finally(() => setLoading(false));
  }, []);

  return (
    <div className={styles.container}>
      <span className={styles.total}>Total: {answers.length}</span>
      {loading ? <Spin /> : answers.map(answer => <SimpleQuestion answer={answer} key={answer.id} />)}
    </div>
  );
};

export default SimpleAnswersList;