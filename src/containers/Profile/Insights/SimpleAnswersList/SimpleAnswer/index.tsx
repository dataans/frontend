
import { CaretDownOutlined, CaretUpOutlined, UpOutlined } from '@ant-design/icons';
import { Tooltip } from 'antd';
import React from 'react';

import { SimpleAnswer as Answer, VoteType } from '../../../../../types/answer';

import styles from './styles.module.sass';

export interface SimpleAnswerProps {
  answer: Answer;
}

const getVotesComponent = (votes: number, isVoted: VoteType | undefined): any => {
  if (isVoted) {
    if (isVoted === VoteType.UP) {
      return <span><CaretUpOutlined /> {votes}</span>;
    } else {
      return <span><CaretDownOutlined /> {votes}</span>;
    }
  } else {
    return <span><UpOutlined /> {votes}</span>;
  }
}

const SimpleQuestion: React.FC<SimpleAnswerProps> = ({ answer }) => {
  return (
    <div className={styles.card}>
      <span>{answer.text}</span>
      <div className={styles.info}>
        <Tooltip title="Votes">
          {getVotesComponent(answer.votes, answer.isVoted)}
        </Tooltip>
        <span>Created at {answer.createdAt.toLocaleString()}</span>
      </div>
    </div>
  );
};

export default SimpleQuestion;