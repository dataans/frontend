
import { EyeOutlined } from '@ant-design/icons';
import { Tooltip } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';

import { SimpleQuestion as Question } from '../../../../../types/question';

import styles from './styles.module.sass';

export interface SimpleQuestionProps {
  question: Question;
}

const SimpleQuestion: React.FC<SimpleQuestionProps> = ({ question }) => {
  return (
    <Link to={{ pathname: `/question/${question.id}` }} className={styles.card}>
      <span className={styles.title}>{question.title}</span>
      <span>{question.text}</span>
      <div className={styles.info}>
        <Tooltip title="Views">
          <span><EyeOutlined /> {question.views}</span>
        </Tooltip>
        <span>Created at {question.createdAt.toLocaleString()}</span>
      </div>
    </Link>
  );
};

export default SimpleQuestion;