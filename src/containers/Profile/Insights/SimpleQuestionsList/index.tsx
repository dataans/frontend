import { Spin } from 'antd';
import React, { useState, useEffect } from 'react';

import { SimpleQuestion as Question } from '../../../../types/question';
import SimpleQuestion from './SimpleQuestion'; 

import styles from './styles.module.sass';

export interface SimpleQuestionsListProps {
  loadQuesions: () => Promise<Question[]>;
}

const SimpleQuestionsList: React.FC<SimpleQuestionsListProps> = ({ loadQuesions }) => {
  const [questions, setQuestions] = useState<Question[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    setLoading(true);
    loadQuesions()
      .then(questions => setQuestions(questions))
      .finally(() => setLoading(false));
  }, []);

  return (
    <div className={styles.container}>
      <span className={styles.total}>Total: {questions.length}</span>
      {loading ? <Spin /> : questions.map(question => <SimpleQuestion question={question} key={question.id} />)}
    </div>
  );
};

export default SimpleQuestionsList;