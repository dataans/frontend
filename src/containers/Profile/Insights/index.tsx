import React, { useEffect, useState } from 'react';
import {
  CaretUpOutlined,
  CommentOutlined,
  MenuUnfoldOutlined,
  PlusSquareOutlined,
  QuestionCircleOutlined,
  SaveOutlined,
} from '@ant-design/icons';

import { loadCreatedAnswers, loadVotedAnswers } from '../../../helpers/api/answer';
import { loadCreatedComments, loadVotedComments } from '../../../helpers/api/comment';
import { loadCreatedQuestions, loadSavedQuestions, loadVotedQuestions } from '../../../helpers/api/question';
import { useUserStore } from '../../../helpers/userStore';
import SimpleAnswersList from './SimpleAnswersList';
import SimpleCommentList from './SimpleCommensList';
import SimpleQuestionsList from './SimpleQuestionsList';

import styles from './styles.module.sass';

export type TabName = "savedQuestions" | "createdQuestions" | "votedQuestions" | "createdAnswers" | "votedAnswers" | "createdComments" | "votedComments";

const tabs: TabName[] = ["savedQuestions", "createdQuestions", "votedQuestions", "createdAnswers", "votedAnswers", "createdComments", "votedComments"];

const getTabNameComponent = (tabName: TabName): any => {
  if (tabName === "savedQuestions") {
    return <span><SaveOutlined /> <QuestionCircleOutlined /> Saved questions</span>;
  }
  if (tabName === "createdQuestions") {
    return <span><PlusSquareOutlined /> <QuestionCircleOutlined /> Created questions</span>;
  }
  if (tabName === "votedQuestions") {
    return <span><CaretUpOutlined /> <QuestionCircleOutlined /> Voted questions</span>;
  }
  if (tabName === "createdAnswers") {
    return <span><PlusSquareOutlined /> <MenuUnfoldOutlined /> Created answers</span>;
  }
  if (tabName === "votedAnswers") {
    return <span><CaretUpOutlined /> <MenuUnfoldOutlined /> Voted answers</span>;
  }
  if (tabName === "createdComments") {
    return <span><PlusSquareOutlined /> <CommentOutlined /> Created comments</span>;
  }
  if (tabName === "votedComments") {
    return <span><CaretUpOutlined /> <CommentOutlined /> Voted comments</span>;
  }
}

const getPanelItemClassFn = (currentTabName: TabName) => (tabName: TabName) =>
  currentTabName === tabName ? styles.panelItemSelected : styles.panelItem;

const Insights: React.FC = ({ }) => {
  const [tab, setTab] = useState<TabName>("savedQuestions");

  const user = useUserStore(state => state.user);

  const getPanelItemClass = getPanelItemClassFn(tab);

  useEffect(() => {
    document.title = 'Insights - Dataans';
  }, []);

  return (
    <div className={styles.container}>
      <div className={styles.leftPanel}>
        {tabs.map(tab =>
          <span className={getPanelItemClass(tab)} onClick={() => setTab(tab)}>{getTabNameComponent(tab)}</span>
        )}
      </div>
      {tab === "savedQuestions" && <SimpleQuestionsList loadQuesions={loadSavedQuestions} />}
      {tab === "createdQuestions" && <SimpleQuestionsList loadQuesions={async () => loadCreatedQuestions(user.id)} />}
      {tab === "votedQuestions" && <SimpleQuestionsList loadQuesions={loadVotedQuestions} />}
      {tab === "createdAnswers" && <SimpleAnswersList loadAnswers={async () => loadCreatedAnswers(user.id)} />}
      {tab === "votedAnswers" && <SimpleAnswersList loadAnswers={loadVotedAnswers} />}
      {tab === "createdComments" && <SimpleCommentList loadComments={async () => loadCreatedComments(user.id)} />}
      {tab === "votedComments" && <SimpleCommentList loadComments={loadVotedComments} />}
    </div>
  );
};

export default Insights;