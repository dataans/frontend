import { Spin } from 'antd';
import React, { useState, useEffect } from 'react';

import { Comment } from '../../../../types/comment';
import SimpleComment from './SimpleComment'; 

import styles from './styles.module.sass';

export interface SimpleCommentsListProps {
  loadComments: () => Promise<Comment[]>;
}

const SimpleCommentList: React.FC<SimpleCommentsListProps> = ({ loadComments }) => {
  const [comments, setComments] = useState<Comment[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    setLoading(true);
    loadComments()
      .then(comments => setComments(comments))
      .finally(() => setLoading(false));
  }, []);

  return (
    <div className={styles.container}>
      <span className={styles.total}>Total: {comments.length}</span>
      {loading ? <Spin /> : comments.map(comment => <SimpleComment comment={comment} key={comment.id} />)}
    </div>
  );
};

export default SimpleCommentList;