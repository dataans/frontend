
import { CaretUpOutlined, EyeOutlined, UpOutlined } from '@ant-design/icons';
import { Tooltip } from 'antd';
import React from 'react';

import { Comment } from '../../../../../types/comment';

import styles from './styles.module.sass';

export interface SimpleCommentProps {
  comment: Comment;
}

const SimpleComment: React.FC<SimpleCommentProps> = ({ comment }) => {
  return (
    <div className={styles.card}>
      <span>{comment.text}</span>
      <div className={styles.info}>
        <Tooltip title="Votes">
          <span>{comment.isVoted ? <CaretUpOutlined /> : <UpOutlined />} {comment.upVotes}</span>
        </Tooltip>
        <span>Created at {comment.createdAt.toLocaleString()}</span>
      </div>
    </div>
  );
};

export default SimpleComment;