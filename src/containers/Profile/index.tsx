import React, { useEffect } from 'react';
import {
  SecurityScanOutlined,
  LineChartOutlined,
  UserOutlined,
  WarningOutlined,
} from '@ant-design/icons';
import { Link } from 'react-router-dom';

import Header from '../../components/Header';
import Account from './Account';
import Security from './Security';
import Danger from './Danger';
import Insights from './Insights';
import { loadUserData, useUserStore } from '../../helpers/userStore';

import styles from './styles.module.sass';

export type TabName = "account" | "insights" | "security" | "danger";

export interface ProfileProps {
  tab: TabName;
}

const getPanelItemClassFn = (currentTabName: TabName) => (tabName: TabName) =>
  currentTabName === tabName ? styles.panelItemSelected : styles.panelItem;

const Profile: React.FC<ProfileProps> = ({ tab }) => {
  const user = useUserStore(state => state.user);
  const setUser = useUserStore(state => state.setUser);

  const getPanelItemClass = getPanelItemClassFn(tab);

  useEffect(() => {
    loadUserData().then(user => {
      setUser(user);
    }).catch(e => console.error(e));
  }, []);

  return (
    <div className={styles.profilePage}>
      <Header user={user} />
      <div className={styles.page}>
        <div className={styles.leftPanel}>
          <div className={styles.panelUser}>
            <img src={user.avatarUrl} />
            <span>{user.fullName}</span>
          </div>
          <Link to={{ pathname: '/user/account' }} className={getPanelItemClass("account")}>
            <UserOutlined />
            <span>Account</span>
          </Link>
          <Link to={{ pathname: '/user/insights' }} className={getPanelItemClass("insights")}>
            <LineChartOutlined />
            <span>Insights</span>
          </Link>
          <Link to={{ pathname: '/user/security' }} className={getPanelItemClass("security")}>
            <SecurityScanOutlined />
            <span>Security</span>
          </Link>
          <div className={styles.panelDivider} />
          <Link to={{ pathname: '/user/danger' }} className={`${getPanelItemClass("danger")} ${styles.redPanelItem}`}>
            <WarningOutlined />
            <span>Danger</span>
          </Link>
        </div>
        <div>
          {tab === "account" && <Account user={user} />}
          {tab === "security" && <Security user={user} setUser={setUser} />}
          {tab === "danger" && <Danger user={user} />}
          {tab === "insights" && <Insights />}
        </div>
      </div>
    </div>
  );
};

export default Profile;