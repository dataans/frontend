import { Button, message } from 'antd';
import React from 'react';
import base32Encode from 'base32-encode';
import { QRCodeSVG } from 'qrcode.react';

import { RecoveryCodeResponse, UserFull } from '../../../../types/user';

import styles from './styles.module.sass';
import { RedoOutlined } from '@ant-design/icons';

export interface TwoFaProps {
  recoveryCodes: RecoveryCodeResponse;
  user: UserFull,
  regenerateCodes: () => void;
}

const TwoFaAuthSettings: React.FC<TwoFaProps> = ({ user, recoveryCodes, regenerateCodes }) => {
  const url = `otpauth://totp/Dataans:${user.primaryEmail}?secret=${base32Encode(new Uint8Array(recoveryCodes.secret.split('').map(c => c.charCodeAt(0))), 'RFC4648')}&issuer=Dataans`;
  
  return (
      <div className={styles.container}>
        <div className={styles.horizontal}>
          {recoveryCodes.secret &&
            <QRCodeSVG value={url} size={200} />
          }
          {recoveryCodes.codes.length !== 0 &&
            <div className={styles.recoveryCodes}>
              <span>// if you lost your device you can use those codes</span>
              <span>// save them. later those codes will be unavailable</span>
              {recoveryCodes.codes.map(code => <span key={code}>{code}</span>)}
            </div>
          }
        </div>
        {recoveryCodes.secret &&
          <span className={styles.secret}>{url}</span>
        }
        <div className={styles.horizontal}>
          {recoveryCodes.secret &&
            <Button type="primary" size="small" onClick={() => navigator.clipboard.writeText(url).then(() => message.success('Link copied!'))}>
              <span>Copy code</span>
            </Button>
          }
          <Button type="primary" size="small" onClick={regenerateCodes} icon={<RedoOutlined />}>
            <span>Regenerate recovery codes</span>
          </Button>
        </div>
      </div>
  );
};

export default TwoFaAuthSettings;