import { CheckCircleOutlined, DeleteOutlined, KeyOutlined, MailOutlined } from '@ant-design/icons';
import { Button, Form, Input, Select, Spin, Switch, Tooltip } from 'antd';
import React, { useState, useEffect } from 'react';

import AskPassword from '../../../components/modal/AskPassword';
import { emptyRecoveryCodes, RecoveryCodeResponse, UserFull } from '../../../types/user';
import { emailRegex } from '../../SignUp';
import { addUserEmail, changeUserPassword, deleteUserEmail, enableUser2Fa, regenerateUserRecoveryCodes, setPrimaryUserEmail } from './actions';
import TwoFaAuthSettings from './TwoFactorAuthSettings';

import styles from './styles.module.sass';

export interface SecurityProps {
  user: UserFull;
  setUser: (u: UserFull) => void;
}

const Security: React.FC<SecurityProps> = ({ user, setUser }) => {
  const [newEmail, setNewEmail] = useState<string>('');
  const [newPassword, setNewPassword] = useState<string>('');
  const [newPasswordRepeat, setPasswordRepeat] = useState<string>('');

  const [confirm, setConfirm] = useState<boolean>(false);
  const [confirmFn, setConfirmFn] = useState<(password: string) => void>(() => () => console.log('fn'));

  const [addEmailLoading, setAddEmailLoading] = useState<boolean>(false);
  const [changePasswordLoading, setChangePasswordLoading] = useState<boolean>(false);
  const [isEmailLoading, setIsEmailLoading] = useState<string>('');

  const [recoveryCodes, setRecoveryCodes] = useState<RecoveryCodeResponse>(emptyRecoveryCodes());

  useEffect(() => {
    document.title = 'Security settings - Dataans';
  }, []);

  return (
    <div className={styles.securityPage}>
      <div className={styles.optionContainer}>
        <div className={styles.optionDescription}>
          <span>2FA</span>
          <span>Enable/disable Two-Factor Authentication (2FA)</span>
          <span>Dataans supports only TOTP as a second factor of authentication</span>
          <span>1) Enable 2FA</span>
          <span>2) Install a compatible application (LastPass Authenticator, Google Authenticator, etc)</span>
          <span>3) Add a new entry in installed Authenticator by scanning QR code or manually</span>
        </div>
        <div className={styles.optionValue}>
          <Switch
            checked={user.is2faEnabled}
            onChange={enable => {
              setConfirmFn(() => (password: string) => {
                enableUser2Fa({ password, enable }, () => {}, setUser);
                if (enable) {
                  regenerateUserRecoveryCodes({ password }, () => {}, setRecoveryCodes);
                }
                setConfirm(false);
              });
              setConfirm(true);
            }}
          />
          {user.is2faEnabled &&
            <TwoFaAuthSettings
              user={user}
              recoveryCodes={recoveryCodes}
              regenerateCodes={() => {
                setConfirmFn(() => (password: string) => {
                  regenerateUserRecoveryCodes({ password }, () => {}, setRecoveryCodes);
                  setConfirm(false);
                });
                setConfirm(true);
              }}
            />
          }
        </div>
      </div>
      <div className={styles.optionContainer}>
        <div className={styles.optionDescription}>
          <span>Primary email</span>
          <span>Choose your primary email</span>
        </div>
        <div className={styles.optionValue}>
          <div className={styles.emails}>
            {user.emails.map(email =>
              <div key={email} className={`${styles.email} ${user.primaryEmail === email ? styles.primaryEmail : ''}`}>
                <span>{email}</span>
                <div className={styles.emailOptions}>
                  {user.primaryEmail !== email &&
                    <Tooltip title="Set as primary email">
                      <CheckCircleOutlined
                        className={styles.setPrimaryEmailButton}
                        onClick={() => {
                          setConfirmFn(() => (password: string) => {
                            setPrimaryUserEmail({ password, email }, setUser, () => {});
                            setConfirm(false);
                          });
                          setConfirm(true);
                        }}
                      />
                    </Tooltip>
                  }
                  {user.primaryEmail !== email &&
                    <Tooltip title="Delete email">
                      <DeleteOutlined
                        className={styles.deleteEmailButton}
                        onClick={() => {
                          setConfirmFn(() => (password: string) => {
                            deleteUserEmail({ password, email }, setUser, isLoading => isLoading ? setIsEmailLoading(email) : '');
                            setConfirm(false);
                          });
                          setConfirm(true);
                        }}
                      />
                    </Tooltip>
                  }
                </div>
                {isEmailLoading === email && <Spin />}
              </div>
            )}
          </div>
        </div>
      </div>
      <div className={styles.optionContainer}>
        <div className={styles.optionDescription}>
          <span>Add email</span>
          <span>Additional emails can be used for the account recovering</span>
        </div>
        <div className={styles.optionValue}>
          <div className={styles.verticalAddEmail}>
            <Form.Item
              name="email"
              rules={[{ required: true, message: '' }]}
              style={{ "marginBottom": "0.5em" }}
              validateStatus={newEmail.match(emailRegex) ? 'success' : 'error'}
            >
              <Input
                prefix={<MailOutlined />}
                size="middle"
                allowClear
                className={styles.input}
                type="email"
                value={newEmail}
                onChange={event => setNewEmail(event.target.value)}
              />
            </Form.Item>
            <div>
              <Button
                type="primary"
                size="small"
                onClick={() => {
                  setConfirmFn(() => (password: string) => {
                    addUserEmail({ password, email: newEmail}, setUser, setAddEmailLoading);
                    setConfirm(false);
                  });
                  setConfirm(true);
                }}
                loading={addEmailLoading}
                disabled={!newEmail.match(emailRegex)}
                icon={<MailOutlined />}
              >
                <span>Add email</span>
              </Button>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.optionContainer}>
        <div className={styles.optionDescription}>
          <span>Change password</span>
          <span>Set a new password</span>
        </div>
        <div className={styles.optionValue}>
          <div className={styles.changePassword}>
            <div className={styles.vertical}>
              <span>New password</span>
              <Form.Item
                name="password_repeat"
                rules={[{ required: true, message: '' }]}
                validateStatus={newPassword === newPasswordRepeat ? 'success' : 'error'}
              >
                <Input
                  allowClear
                  size="middle"
                  className={styles.input}
                  type="password"
                  onChange={event => setNewPassword(event.target.value)}
                />
              </Form.Item>
            </div>
            <div className={styles.vertical}>
              <span>Repeat new password</span>
              <Form.Item
                name="password_repeat"
                rules={[{ required: true, message: '' }]}
                validateStatus={newPassword === newPasswordRepeat ? 'success' : 'error'}
              >
                <Input
                  allowClear
                  size="middle"
                  className={styles.input}
                  type="password"
                  onChange={event => setPasswordRepeat(event.target.value)}
                />
              </Form.Item>
            </div>
            <div>
              <Button
                type="primary"
                size="small"
                loading={changePasswordLoading}
                disabled={newPassword !== newPasswordRepeat && newPassword === ''}
                onClick={() => {
                  setConfirmFn(() => (password: string) => {
                    changeUserPassword({ password, newPassword }, setChangePasswordLoading);
                    setConfirm(false);
                  });
                  setConfirm(true);
                }}
                icon={<KeyOutlined />}
              >
                <span>Change password</span>
              </Button>
            </div>
            <div>
              <span>Forgot password?</span>
            </div>
          </div>
        </div>
      </div>
      {confirm && <AskPassword onCancel={() => setConfirm(false)} onSubmit={confirmFn} />}
    </div>
  );
};

export default Security;