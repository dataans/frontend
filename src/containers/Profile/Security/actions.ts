import { message } from 'antd';

import { addEmail, changePassword, deleteEmail, enable2Fa, regenerateRecoveryCodes, setPrimaryEmail } from '../../../helpers/api/user';
import { AddEmailRequest, ChangePasswordRequest, DeleteEmailRequest, Enable2Fa, RecoveryCodeRequest, RecoveryCodeResponse, SetPrimaryEmailRequest, UserFull } from '../../../types/user';

export const addUserEmail = async (data: AddEmailRequest, setUser: (u: UserFull) => void, setLoading: (s: boolean) => void) => {
  setLoading(true);
  try {
    setUser(await addEmail(data));
  } catch (e) {
    message.error(e + '');
  } finally {
    setLoading(false);
  }
};

export const deleteUserEmail = async (data: DeleteEmailRequest, setUser: (u: UserFull) => void, setLoading: (s: boolean) => void) => {
  setLoading(true);
  try {
    setUser(await deleteEmail(data));
  } catch (e) {
    message.error(e + '');
  } finally {
    setLoading(false);
  }
};

export const setPrimaryUserEmail = async (data: SetPrimaryEmailRequest, setUser: (u: UserFull) => void, setLoading: (s: boolean) => void) => {
  setLoading(true);
  try {
    setUser(await setPrimaryEmail(data));
  } catch (e) {
    message.error(e + '');
  } finally {
    setLoading(false);
  }
};

export const changeUserPassword = async (data: ChangePasswordRequest, setLoading: (s: boolean) => void) => {
  setLoading(true);
  try {
    await changePassword(data);
    message.success('Password changed!');
  } catch (e) {
    message.error(e + '');
  } finally {
    setLoading(false);
  }
};

export const enableUser2Fa = async (data: Enable2Fa, setLoading: (s: boolean) => void, setUser: (u: UserFull) => void) => {
  setLoading(true);
  try {
    setUser(await enable2Fa(data));
  } catch (e) {
    message.error(e + '');
  } finally {
    setLoading(false);
  }
};

export const regenerateUserRecoveryCodes = async (data: RecoveryCodeRequest, setLoading: (s: boolean) => void, setCodes: (u: RecoveryCodeResponse) => void) => {
  setLoading(true);
  try {
    setCodes(await regenerateRecoveryCodes(data));
  } catch (e) {
    message.error(e + '');
  } finally {
    setLoading(false);
  }
};
