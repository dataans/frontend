import React, { useState, useEffect } from 'react';
import { ContactsOutlined, UserOutlined, UploadOutlined, SaveOutlined, UpSquareOutlined } from '@ant-design/icons';
import { Button, Input, message } from 'antd';

import { updateAccountData } from '../../../helpers/api/user';
import { useUserStore } from '../../../helpers/userStore';
import { UpdateUserAccountDataRequest, UserFull } from '../../../types/user';

import styles from './styles.module.sass';

export interface AccountProps {
  user: UserFull;
}

const updateUserAccountData = async (data: UpdateUserAccountDataRequest, setUser: (u: UserFull) => void, setLoading: (l: boolean) => void) => {
  setLoading(true);
  try {
    const user = await updateAccountData(data);

    setUser(user);

    message.success('User data updated!');
  } catch (e) {
    message.error(e + '');
  } finally {
    setLoading(false);
  }
};

const updateUserAvatar = async (file: any, setUser: (u: UserFull) => void, setLoading: (l: boolean) => void, setFile: (f: any) => void) => {
  setLoading(true);
  try {
    const data = new FormData();

    console.log(file);
    data.append('avatar', file, file.name);

    const response = await fetch('https://api.dataans.com/api/v1/user/avatar', {
        method: 'POST',
        body: data,
        credentials: 'include',
    });
    const user = await response.json() as UserFull;

    setUser(user);
    setFile(null);

    message.success('User avatar updated!');
  } catch (e) {
    message.error(e + '');
  } finally {
    setLoading(false);
  }
};

const Account: React.FC<AccountProps> = ({ user }) => {
  const setUser = useUserStore(state => state.setUser);

  const [username, setUsername] = useState<string>(user.username);
  const [fullName, setFullName] = useState<string>(user.fullName);
  const [avatarLoading, setAvatarLoading] = useState<boolean>(false);
  const [userDataLoading, setUserDataLoading] = useState<boolean>(false);
  const [file, setFile] = useState<any>(null);

  useEffect(() => {
    document.title = 'Profile settings - Dataans';
  }, []);

  useEffect(() => {
    setUsername(user.username);
    setFullName(user.fullName);
  }, [user]);

  return (
    <div className={styles.accountPage}>
      <div className={styles.optionContainer}>
        <div className={styles.optionDescription}>
          <span>Name</span>
          <span>Everyone will see it and it'll appear near your questions, answers and comments</span>
        </div>
        <div className={styles.optionValue}>
          <Input
            prefix={<ContactsOutlined />}
            size="middle"
            allowClear
            className={styles.input}
            value={fullName}
            onChange={event => setFullName(event.target.value)}
          />
        </div>
      </div>
      <div className={styles.optionContainer}>
        <div className={styles.optionDescription}>
          <span>Username</span>
          <span>Must be unique for all users</span>
        </div>
        <div className={styles.optionValue}>
          <Input
            prefix={<UserOutlined />}
            size="middle"
            allowClear
            className={styles.input}
            value={username}
            onChange={event => setUsername(event.target.value)}
          />
        </div>
      </div>
      <div className={styles.pageActions}>
        <Button
          type="primary"
          size="middle"
          onClick={() => updateUserAccountData({ username, fullName }, setUser, setUserDataLoading)}
          icon={<SaveOutlined />}
          loading={userDataLoading}
        >
          <span>Update account</span>
        </Button>
      </div>
      <div className={styles.optionContainer}>
        <div className={styles.optionDescription}>
          <span>Avatar</span>
          <span>Change your public account profile photo</span>
          <span>Max file size: 1Mb</span>
        </div>
        <div className={styles.optionValue}>
          <img src={user.avatarUrl} />
          <div className={styles.vertical}>
            <input type="file" multiple name="file" accept="images/*" className={styles.uploadInput} onChange={event => {
              if (event?.target?.files) {
                setFile(event.target.files[0])
              }
              }}
            />
            {file &&
              <div>
                <Button
                  type="primary"
                  icon={<SaveOutlined />}
                  size="small"
                  onClick={() => updateUserAvatar(file, setUser, setAvatarLoading, setFile)}
                  loading={avatarLoading}
                >
                  <span>Save</span>
                </Button>
              </div>
            }
          </div>
        </div>
      </div>
      <div className={styles.divider} /> 
      <div className={styles.optionContainer}>
        <div className={styles.optionDescription}>
          <span>Export data</span>
          <span>Export all account data</span>
        </div>
        <div className={styles.optionValue}>
          <Button type="primary" icon={<UpSquareOutlined />} disabled>
            <span>Export</span>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Account;