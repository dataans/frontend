import React from 'react';
import { Space } from 'antd';

export interface HorizontalListProps<T> {
  items: T[];
  Component: any;
}

const HorizontalList: React.FC<HorizontalListProps<any>> = ({ items, Component }) => {
  return (
    <Space direction="horizontal" size="small">
      {items?.map(e => <Component {...e} />)}
    </Space>
  );
};

export default HorizontalList;