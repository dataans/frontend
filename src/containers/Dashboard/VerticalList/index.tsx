import React from 'react';
import { Space } from 'antd';

export interface VerticalListProps<T> {
  items: T[];
  Component: any;
  maxWidth?: string;
}

const VerticalList: React.FC<VerticalListProps<any>> = ({ items, Component, maxWidth }) => {
  return (
    <Space direction="vertical" size="middle" style={{ width: maxWidth }}>
      {items?.map(e => <Component {...e} />)}
    </Space>
  );
};

export default VerticalList;
