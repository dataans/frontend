import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';

import Header from '../../components/Header';
import Card, { CardProps } from '../../components/Card';
import HorizontalList from './HorizontalList';
import HorizontalQuestionCard, { HorizontalQuestionCardProps } from '../../components/HorizontalQuestionCard';
import VerticalList from './VerticalList';
import { loadUserData, useUserStore } from '../../helpers/userStore';

import styles from './styles.module.sass';

const tags: CardProps[] = [
  {
    id: '1',
    title: 'PostgreSQL',
    description: '321',
    imgUrl: 'https://cdn.holistics.io/landing/databases/postgresql.png',
  },
  {
    id: '2',
    title: 'MySQL',
    description: '83',
    imgUrl: 'https://pbs.twimg.com/profile_images/1255113654049128448/J5Yt92WW_400x400.png',
  },
  {
    id: '3',
    title: 'MSSQL',
    description: '137',
    imgUrl: 'https://allvectorlogo.com/img/2017/02/microsoft-sql-server-logo.png',
  },
  {
    id: '4',
    title: 'sql_joins',
    description: '32',
    imgUrl: 'https://www.metabase.com/learn/images/sql-join-types/join-types.png',
  },
  {
    id: '1',
    title: 'PostgreSQL',
    description: '321',
    imgUrl: 'https://cdn.holistics.io/landing/databases/postgresql.png',
  },
  {
    id: '2',
    title: 'MySQL',
    description: '83',
    imgUrl: 'https://pbs.twimg.com/profile_images/1255113654049128448/J5Yt92WW_400x400.png',
  },
  {
    id: '3',
    title: 'MSSQL',
    description: '137',
    imgUrl: 'https://allvectorlogo.com/img/2017/02/microsoft-sql-server-logo.png',
  },
  {
    id: '4',
    title: 'sql_joins',
    description: '32',
    imgUrl: 'https://www.metabase.com/learn/images/sql-join-types/join-types.png',
  },
];

const questions: HorizontalQuestionCardProps[] = [
];

const Dashboard: React.FC = () => {
  const user = useUserStore(state => state.user);
  const setUser = useUserStore(state => state.setUser);

  useEffect(() => {
    document.title = 'DataAns Dashboard';
    loadUserData().then(user => {
      setUser(user);
    }).catch(e => console.error(e));
  }, []);

  return (
    <div className={styles.dashboard}>
      <Header user={user} />
      <div className={styles.page}>
        <div className={styles.tagsSection}>
          <Link to={{ pathname: '/tags' }} className={styles.tagsSectionTitle}>EXPLORE TAGS</Link>
          <HorizontalList items={tags} Component={Card} />
        </div>
        <VerticalList items={questions} Component={HorizontalQuestionCard} maxWidth="70%" />
      </div>
    </div>
  )
};

export default Dashboard;