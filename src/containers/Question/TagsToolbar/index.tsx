import React from 'react';
import { Tag as AntTag, Select } from 'antd';
import { Link } from 'react-router-dom';
import { Tag, emptyTag } from '../../../types/tag';

import styles from './styles.module.sass';

export interface TagsToolbarProps {
  edit: boolean;
  tags: Tag[],
  questionTags: Tag[],
  setQuestionTags: (tags: Tag[]) => void;
}

const tagColors = ["blue", "pink", "red", "yellow", "orange", "cyan", "green", "purple", "geekblue", "magenta", "volcano", "gold", "lime"];

const mapTags = (tags: Tag[]) => tags.map(tag => ({ id: tag.id, value: tag.name }));

const mapValuesToTags = (values: any, tags: Tag[]): Tag[] => {
  const res = values.map((value: any) => {
    const tag = tags.find(t => t.name === value);
    if (tag) {
      return { ...tag };
    }
    return emptyTag();
  }).filter((tag: Tag) => tag.id !== '');
  return res;
};

const getRandomColor = () => {
  let index = Math.round(tagColors.length * Math.random());
  return tagColors[index];
};

const TagsToolbar: React.FC<TagsToolbarProps> = ({ edit, tags, questionTags, setQuestionTags }) => {
  return (
    <div className={styles.container}>
      <div className={styles.toolbar}>
        {edit ?
          <Select
            className={styles.tagInput}
            mode="multiple"
            placeholder="Add tags"
            size="middle"
            options={mapTags(tags)}
            value={mapTags(questionTags)}
            onChange={values => setQuestionTags(mapValuesToTags(values, tags))}
          /> :
          questionTags.map(tag =>
            <Link to={{ pathname: `/tag/${tag.id}` }} id={tag.id} key={tag.id}>
              <AntTag color={getRandomColor()} className={styles.tag}>
                #{tag.name}
              </AntTag>
            </Link>
          )}
      </div>
    </div>
  );
};

export default TagsToolbar;