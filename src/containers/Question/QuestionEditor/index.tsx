import React from 'react';
import { Dropdown, Input, Menu } from 'antd';
import MDEditor from '@uiw/react-md-editor';
import { Link } from 'react-router-dom';
import { DeleteOutlined, EditOutlined, MenuOutlined } from '@ant-design/icons';

import { Question } from '../../../types/question';
import { formatDateTime } from '../../../helpers/timeDate';
import { User } from '../../../types/user';

import styles from './styles.module.sass';

export interface QuestionEditorProps {
  user: User;
  question: Question;
  edit: boolean;
  setEdit: (edit: boolean) => void,
  setTitle: (code: string) => void;
  setText: (code: string) => void;
}

const menu = (onEdit: () => void, onDelete: () => void) => (
  <Menu className={styles.menu}>
    <Menu.Item key="1" className={`${styles.menuItem} ${styles.blueMenuItem}`} onClick={onEdit}>
      <div className={styles.menuItemGroup}>
        <EditOutlined />
        <span>Edit</span>
      </div>
    </Menu.Item>
    <Menu.Item key="2" className={`${styles.menuItem} ${styles.redMenuItem}`} onClick={onDelete} disabled>
      <div className={styles.menuItemGroup}>
        <DeleteOutlined />
        <span>Delete</span>
      </div>
    </Menu.Item>
  </Menu>
);

const QuestionEditor: React.FC<QuestionEditorProps> = ({ question, edit, setTitle, setText, user, setEdit }) => {
  return (
    <div className={styles.vertical}>
      {edit ?
        <Input placeholder="Title" size="large" type="text" value={question.title} onChange={e => setTitle(e.target.value)} className={styles.titleInput}/>
        :
        <div className={styles.titleContainer}>
          <span className={styles.title}>{question.title}</span>
          <div className={styles.itemsList}>
            <Link to={{ pathname: `/user/${question.creator.id}` }} className={styles.author}>
              <img src={question.creator.avatarUrl} />
              <span>{question.creator.fullName}</span>
            </Link>
            <span>created: {formatDateTime(question.createdAt)}</span>
            <span>updated: {formatDateTime(question.updatedAt)}</span>
            {question.creator.id === user.id &&
              <Dropdown overlay={menu(() => setEdit(true), () => {})}>
                <MenuOutlined />
              </Dropdown>
            }
          </div>
        </div>
      }
      <MDEditor
        value={question.text}
        onChange={e => setText(e || '')}
        preview={edit ? "live" : "preview"}
        hideToolbar={!edit}
      />
    </div>
  );
};

export default QuestionEditor;