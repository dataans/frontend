import React from 'react';
import { message, Tooltip } from 'antd';
import {
  UpCircleFilled,
  CommentOutlined,
  EyeOutlined,
  UpCircleOutlined,
  BookOutlined,
  BookFilled,
  ShareAltOutlined,
} from '@ant-design/icons';

import { Question } from '../../../types/question';

import styles from './styles.module.sass';

export interface QuestionOptionsProps {
  edit: boolean;
  question: Question;
  vote: (id: number) => void;
  addToSaved: (id: number) => void;
}

const QuestionOptions: React.FC<QuestionOptionsProps> = ({ edit, question, vote, addToSaved }) => {
  return (
    <div className={styles.container}>
      <div className={`${styles.options} ${edit ? styles.disabled : ''}`}>
        <Tooltip className={styles.optionsButton} title="Yea, it's a good one">
          <span onClick={() => vote(question.id)}>
            {question.isVoted ? <UpCircleFilled /> : <UpCircleOutlined />} {question.upVotes} 
          </span>
        </Tooltip>
        <Tooltip className={styles.optionsButton} title="Maybe I'll need it in the feature">
          <span onClick={() => addToSaved(question.id)}>
            {question.isSaved ? <BookFilled /> : <BookOutlined />} {question.savedTimes}
          </span>
        </Tooltip>
        <span><EyeOutlined /> {question.views}</span>
        <span><CommentOutlined /> {question.comments.length}</span>
        {!edit &&
          <span className={styles.share} onClick={() => navigator.clipboard.writeText(window.location.href).then(() => message.success('Link copied!'))}>
            <ShareAltOutlined />
          </span>
        }
      </div>
    </div>
  );
};

export default QuestionOptions;