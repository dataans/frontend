import React, { useState, useEffect } from 'react';
import { Button, message, Switch, Tooltip } from 'antd';
import { useParams } from 'react-router-dom';
import { ConsoleSqlOutlined, SaveOutlined } from '@ant-design/icons';

import Header from '../../components/Header';
import { useQuestionStore } from '../../helpers/questionStore';
import { loadUserData, useUserStore } from '../../helpers/userStore';
import { Question } from '../../types/question';
import QuestionEditor from './QuestionEditor';
import SqlCodeEditor from '../../components/SqlCodeEditor';
import QuestionOptions from './QuestionOptions';
import TagsToolbar from './TagsToolbar';
import Comment from '../../components/Comment';
import { CommentType, emptyFullComment } from '../../types/comment';
import Answer from '../../components/Answer';
import { AnswerFull, emptyFullAnswer } from '../../types/answer';
import { loadSample } from '../../helpers/api/sample';
import { loadAnswers } from '../../helpers/api/answer';
import { addToSaved, loadQuestion, loadTags, vote } from '../../helpers/api/question';
import {
  createQuestionSample,
  deleteQuestionComment,
  deleteQuestionSample,
  publishAnswer,
  publishComment,
  saveSample,
  submitQuestion,
  updateQuestionAnswer,
  updateQuestionComment,
  voteQuestionComment,
} from './actions';
import { Sample } from '../../types/sample';

import styles from './styles.module.sass';

async function updateQuestion<T extends CallableFunction>(call: T, update: (question: Question) => void) {
  const newQuestion = await call();
  update(newQuestion);
}

const onSwitchChange = async (checked: boolean, sample: Sample, question: Question, setSample: (s: Sample) => void, setQuestion: (q: Question) => void) => {
  if (checked) {
    await createQuestionSample(sample, question, setSample, setQuestion);
  } else {
    await deleteQuestionSample(sample, question, setSample, setQuestion);
  }
};

export interface QuestionProps {
}

const QuestionPage: React.FC<QuestionProps> = ({ }) => {
  const [isSampleEdited, setIsSampleEdited] = useState<boolean>(false);
  const [edit, setEdit] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const params = useParams();

  const question = useQuestionStore(state => state.question);
  const setQuestion = useQuestionStore(state => state.setQuestion);

  const [localQuestion, setLocalQuestion] = useState<Question>(question);
  const setTitle = (title: string, localQuestion: Question) => setLocalQuestion({ ...localQuestion, title });
  const setText = (text: string, localQuestion: Question) => setLocalQuestion({ ...localQuestion, text });

  const sample = useQuestionStore(state => state.sample);
  const setSample = useQuestionStore(state => state.setSample);
  const setQuery = useQuestionStore(state => state.setQueryCode);
  const setSchema = useQuestionStore(state => state.setSchemaCode);

  const tags = useQuestionStore(state => state.tags);
  const setTags = useQuestionStore(state => state.setTags);
  const setQuestionTags = useQuestionStore(state => state.setQuestionTags);

  const answers = useQuestionStore(state => state.answers);
  const setAnswers = useQuestionStore(state => state.setAnswers);
  const setAnswer = useQuestionStore(state => state.setAnswer);

  const user = useUserStore(state => state.user);
  const setUser = useUserStore(state => state.setUser);

  useEffect(() => {
    loadUserData().then(user => {
      setUser(user);
    }).catch(e => console.error(e));
    loadTags().then(tags => {
      setTags(tags);
    }).catch(e => console.error(e));
  }, []);

  useEffect(() => {
    document.title = `${question.title.substring(0, 10)} - DataAns`;
    if (question.sampleId) {
      loadSample(question.sampleId)
        .then(data => setSample(data))
        .catch(e => message.error(e + ''));
    }
    setLocalQuestion({ ...question });
  }, [question]);

  useEffect(() => {
    const id = params.id;
    setEdit(false);

    if (id && id !== 'new') {
      setLoading(true);

      loadQuestion(parseInt(id) || 0)
        .then(data => setQuestion(data))
        .catch(e => message.error(e + ''))
        .finally(() => setLoading(false));

      loadAnswers(parseInt(id) || 0)
        .then(data => setAnswers(data))
        .catch(e => message.error(e + ''))
    }
    if (id === 'new') {
      setEdit(true);
    }
  }, [params]);

  return (
    <div className={styles.questionPage}>
      <Header user={user} />
      <div className={styles.page}>
        <div className={styles.question}>
          <QuestionOptions edit={edit} question={localQuestion} vote={id => updateQuestion(async () => vote(id), setQuestion)} addToSaved={id => updateQuestion(async () => addToSaved(id), setQuestion)} />
          <div className={styles.questionMain}>
            <QuestionEditor question={localQuestion} setTitle={title => setTitle(title, localQuestion)} setText={text => setText(text, localQuestion)} edit={edit} setEdit={setEdit} user={user} />
            {((sample.creatorId === user.id && user.id) || (!sample.id && !question.id) || (question.creator.id === user.id)) &&
              <div className={styles.questionOptions}>
                <ConsoleSqlOutlined style={{ "fontSize": "0.75em" }} />
                <Tooltip title={sample.id ? "Remove SQL code sample" : "Add SQL code sample"}>
                  <Switch
                    style={{ width: "fit-content", backgroundColor: "#24233b" }}
                    checked={sample.id !== ''}
                    onChange={(checked, _) => onSwitchChange(checked, sample, question, setSample, setQuestion)}
                  />
                </Tooltip>
                {isSampleEdited &&
                  <Tooltip title="Save code sample without execution">
                    <SaveOutlined className={styles.questionOptionsIcon} onClick={() => {
                      saveSample(sample, setSample);
                      setIsSampleEdited(false);
                    }}/>
                  </Tooltip>
                }
              </div>
            }
            {sample.id && 
              <SqlCodeEditor
                sample={sample}
                setQueryCode={query => {
                  setQuery(query);
                  setIsSampleEdited(true);
                }}
                setSchemaCode={schema => {
                  setSchema(schema);
                  setIsSampleEdited(true);
                }}
                edit={edit || sample.creatorId === user.id}
              />
            }
            <div className={styles.questionMain}>
              {edit &&
                <div className={styles.horizontal}>
                  <Button type="primary" size="small" danger ghost onClick={() => {
                    if (question.id) {
                      setEdit(false);
                      setTitle(question.title, question);
                      setText(question.text, question);
                    }
                  }}>
                    <span>Discard</span>
                  </Button>
                  <Button type="primary" size="small" onClick={() => submitQuestion(localQuestion, sample, setQuestion, setLoading, setEdit)} loading={loading}>
                    <span>Publish</span>
                  </Button>
                </div>
              }
            </div>
            <div className={styles.comments}>
              {question.comments.sort((c1, c2) => c1.createdAt.getTime() - c2.createdAt.getTime()).map(comment =>
                <Comment
                  key={comment.id}
                  comment={comment}
                  onSubmit={comment => updateQuestionComment(comment, question.id, setQuestion)}
                  onVote={commentId => voteQuestionComment(commentId, question.id, setQuestion)}
                  onDelete={commentId => deleteQuestionComment(commentId, question.id, setQuestion)}
                />
              )} 
              {question.id &&
                <Comment
                  comment={emptyFullComment(user)}
                  onSubmit={comment => publishComment({
                    commentType: CommentType.QUESTION, 
                    parentId: question.id.toString(),
                    text: comment.text,
                  }, setQuestion)}
                  onVote={() => {}}
                  onDelete={() => {}}
                />
              }
            </div>
            {answers.sort((a1: AnswerFull, a2: AnswerFull) => a1.createdAt.getTime() - a2.createdAt.getTime()).map(answer =>
              <Answer
                key={(answer as AnswerFull).id}
                answer={answer}
                onSubmit={data => updateQuestionAnswer({
                  id: data.id,
                  text: data.text,
                  sampleId: data.sample?.id === '' ? undefined : data.sample?.id,
                  parentId: question.id + '',
                }, setAnswers)}
                onAnswerChange={setAnswer}
              />
            )}
            {question.id !== 0 && user.id &&
              <div>
                <Answer
                  answer={emptyFullAnswer(user)}
                  onSubmit={data => publishAnswer({ text: data.text, parentId: question.id, sample: data.sample }, setAnswers)}
                  onAnswerChange={() => {}}
                />
              </div>
            }
          </div>
          <TagsToolbar edit={edit} tags={tags} questionTags={question.tags} setQuestionTags={setQuestionTags} />
        </div>
      </div>
    </div>
  );
};

export default QuestionPage;