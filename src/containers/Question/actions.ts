import { message } from 'antd';
import { createAnswer, loadAnswers, updateAnswer } from '../../helpers/api/answer';
import { deleteComment, updateComment, voteComment } from '../../helpers/api/comment';
import { createComment, createQuestion, loadQuestion, updateQuestion } from '../../helpers/api/question';
import { createSample, deleteSample, updateSample } from '../../helpers/api/sample';
import { AnswerCreate, AnswerFull, AnswerUpdate } from '../../types/answer';
import { CommentCreate, CommentUpdate } from '../../types/comment';
import { Question, questionToQuestionCreate } from '../../types/question';
import { emptySample, Sample, sampleToCreateSample } from '../../types/sample';

export const publishQuestion = async (questionData: Question, sampleData: Sample, setQuestion: (q: Question) => void, setLoading: (v: boolean) => void, setEdit: (edit: boolean) => void) => {
  if (questionData.text === '') {
    message.error('Question text is empty');
    return;
  }
  if (questionData.title === '') {
    message.error('Question title is empty');
    return;
  }
  setLoading(true);
  try {
    if (sampleData.id) {
      await updateSample(sampleData);
    }
    questionData.sampleId = sampleData.id ? sampleData.id : undefined;

    const question = await createQuestion(questionToQuestionCreate(questionData));

    message.success('Question created successfully');

    setQuestion(question);

    const id = question.id + '';
    window.history.pushState(id, id, id);
    setEdit(false);
  } catch (e) {
    message.error(e + '');
  }
  setLoading(false);
};

export const updateQuestionAction = async (questionData: Question, sampleData: Sample, setQuestion: (q: Question) => void, setLoading: (v: boolean) => void, setEdit: (edit: boolean) => void) => {
  if (questionData.text === '') {
    message.error('Question text is empty');
    return;
  }
  if (questionData.title === '') {
    message.error('Question title is empty');
    return;
  }
  setLoading(true);
  try {
    const question = await updateQuestion({
      id: +questionData.id,
      title: questionData.title,
      text: questionData.text,
      sampleId: questionData.sampleId,
      tags: questionData.tags.map(t => t.id),
    });

    message.success('Question updated successfully');

    setQuestion(question);
    setEdit(false);
  } catch (e) {
    message.error(e + '');
  }
  setLoading(false);
};

export const submitQuestion = async (questionData: Question, sampleData: Sample, setQuestion: (q: Question) => void, setLoading: (v: boolean) => void, setEdit: (edit: boolean) => void) => {
  if (questionData.id) {
    updateQuestionAction(questionData, sampleData, setQuestion, setLoading, setEdit);
  } else {
    publishQuestion(questionData, sampleData, setQuestion, setLoading, setEdit);
  }
};

export const publishComment = async (commentData: CommentCreate, setQuestion: (q: Question) => void) => {
  try {
    const _comment = await createComment(commentData);
    setQuestion(await loadQuestion(+commentData.parentId));

    message.success('Comment added successfully');
  } catch (e) {
    message.error(e + '');
  }
};

export const updateQuestionComment = async (commentData: CommentUpdate, questionId: number, setQuestion: (q: Question) => void) => {
  try {
    const _comment = await updateComment(commentData);
    setQuestion(await loadQuestion(questionId));

    message.success('Comment updated successfully');
  } catch (e) {
    message.error(e + '');
  }
};

export const voteQuestionComment = async (commentId: string, questionId: number, setQuestion: (q: Question) => void) => {
  try {
    const _comment = await voteComment(commentId);

    setQuestion(await loadQuestion(questionId));

    message.success('Vote saved!');
  } catch (e) {
    message.error(e + '');
  }
};

export const deleteQuestionComment = async (commentId: string, questionId: number, setQuestion: (q: Question) => void) => {
  try {
    await deleteComment(commentId);

    setQuestion(await loadQuestion(questionId));
  } catch (e) {
    message.error(e + '');
  }
};

export const publishAnswer = async (answerData: AnswerCreate, setAnswers: (a: AnswerFull[]) => void) => {
  try {
    if (answerData.sample) {
      const _sample = await updateSample(answerData.sample);
    }
    const _answer = await createAnswer(answerData);
    setAnswers(await loadAnswers(answerData.parentId));

    message.success('Answer added successfully');
  } catch (e) {
    message.error(e + '');
  }
};

export const updateQuestionAnswer = async (answerData: AnswerUpdate, setAnswers: (a: AnswerFull[]) => void) => {
  try {
    const _answer = await updateAnswer(answerData);
    setAnswers(await loadAnswers(+answerData.parentId));
  } catch (e) {
    message.error(e + '');
  }
};

export const createQuestionSample = async (sample: Sample, questionData: Question, setSample: (s: Sample) => void, setQuestion: (q: Question) => void) => {
  const { query, schema } = sample;
  message.loading('Please wait...');
  try {
    const sample = await createSample({ query, schema });
    
    if (questionData.id !== 0) {
      const question = await updateQuestion({
        id: +questionData.id,
        title: questionData.title,
        text: questionData.text,
        sampleId: sample.id,
        tags: questionData.tags.map(t => t.id),
      });

      setQuestion(question);
    }

    setSample(sample);
  } catch(e) {
    message.error(e + '');
  }
};

export const deleteQuestionSample = async (sample: Sample, questionData: Question, setSample: (s: Sample) => void, setQuestion: (q: Question) => void) => {
  message.loading('Please wait...');
  try {
    const question = questionData.id ? await updateQuestion({
      id: +questionData.id,
      title: questionData.title,
      text: questionData.text,
      sampleId: undefined,
      tags: questionData.tags.map(t => t.id),
    }) : questionData;
    await deleteSample(sample.id);

    setSample(emptySample());
    setQuestion(question);
  } catch(e) {
    message.error(e + '');
  }
};

export const saveSample = async (sample: Sample, setSample: (s: Sample) => void): Promise<void> => {
  try {
    const updatedSample = await updateSample(sample);
    setSample(updatedSample);

    message.success('Code sample updated!');
  } catch (e) {
    message.error(e + '');
  }
}
