import React, { ChangeEvent, useEffect, useState } from 'react';
import { Form, Input, Button, Divider, Tooltip, message } from 'antd';
import { UserOutlined, LockOutlined, MailOutlined, ContactsOutlined, CheckCircleFilled } from '@ant-design/icons';
import { Link, useNavigate } from 'react-router-dom';
import OAuth from '../../components/OAuth';
import { register } from './apiCalls';
import apiClient from '../../helpers/api';

import styles from './style.module.sass';
import titles from '../../styles/titles.module.sass';

export const emailRegex = /^[a-zA-Z0-9.]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/;
const usernameRegex = /^[a-zA-Z0-9]+([._]?[a-zA-Z0-9]+)*$/;
const fullNameRegex = /^[a-zA-Z]{2,}(?: [a-zA-Z]+){0,2}$/;

interface FormField<T> {
  value: T;
  isValid: string;
}

const onFinish = async (values: any, setLoading: any) => {
  try {
    setLoading(true);
    await register({
        username: values.username,
        email: values.email,
        fullName: values.fullName,
        password: values.password,
      });
    setLoading(false);
    await message.success('Sign up success. You can login now')
  } catch (e) {
    await message.error(e + '');
  }
};

const onPasswordChange = (set: (p: FormField<boolean[]>) => void, setPassword: (p: string) => void) => (e: ChangeEvent<HTMLInputElement>) => {
  let password = e.target.value;

  let len = password.length >= 10;
  let lower = false;
  let upper = false;
  let num = false;
  let special = false;

  for (let i = 0; i < password.length; i++) {
    const c = password.charCodeAt(i);

    if (c >= 65 && c <= 90) {
      upper = true;
    }
    if (c >= 97 && c <= 122) {
      lower = true;
    }
    if (c >= 48 && c <= 57) {
      num = true;
    }
    if ((c >= 33 && c <= 47) || (c >= 58 && c <= 64) || (c >= 91 && c <= 96) || (c >= 123 && c <= 126)) {
      special = true;
    }
  }

  let isValid = 'error';
  if (len && upper && lower && num && special) {
    isValid = 'success';
  }

  set({ value: [len, upper, lower, num, special], isValid });
  setPassword(password);
};

const onPasswordRepeatChange = (set: (p: string) => void, password: string) => (e: ChangeEvent<HTMLInputElement>) => {
  set(password === e.target.value ? 'success' : 'error');
};

const RegisterPage: React.FC = () => {
  let [passwordParameters, setPasswordParameters] = useState<FormField<boolean[]>>({ value: [false, false, false, false, false], isValid: 'error' });
  let [password, setPassword] = useState<string>('');
  let [passwordRepeat, setPasswordRepeat] = useState<string>('error');
  let [isLoading, setLoading] = useState<boolean>(false);

  const navigate = useNavigate();

  useEffect(() => {
    document.title = 'Sign up - DataAns'

    const url = window.location.href;
    const hasCode = url.includes("?code=");

    if (hasCode) {
      const code = url.split("?code=")[1];

      setLoading(true);
      apiClient.post({ endpoint: '/auth/github', body: { code } }).then(res => {
        navigate('/');
      }).catch(async err => {
        message.error(err + '');
      }).finally(() => setLoading(false));
    }
  }, []);

  const getCircleStyle = (i: number) => passwordParameters.value[i] ? styles.circleChecked : styles.circleUnchecked;

  return (
    <div className={styles.registerPage}>
      <div className={styles.registerForm}>
        <div className={styles.formInnerWrapper} />
        <Form name="login" onFinish={data => onFinish(data, setLoading)} >
          <Form.Item>
            <span className={titles.formTitle}>Sign up</span>
          </Form.Item>
          <Form.Item
            name="username"
            rules={[{ required: true, message: '', pattern: usernameRegex }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Username"
              size="large"
              allowClear
              className={styles.input}
            />
          </Form.Item>
          <Form.Item
            name="fullName"
            rules={[{ required: true, message: '', pattern: fullNameRegex }]}
          >
            <Input
              prefix={<ContactsOutlined className="site-form-item-icon" />}
              placeholder="Your Full Name"
              size="large"
              allowClear
              className={styles.input}
            />
          </Form.Item>
          <Form.Item
            name="email"
            rules={[{ required: true, message: '', pattern: emailRegex }]}
          >
            <Input
              prefix={<MailOutlined className="site-form-item-icon" />}
              placeholder="your.email@example.com"
              size="large"
              allowClear
              className={styles.input}
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: '' }]}
            validateStatus={passwordParameters.isValid as any}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password"
              size="large"
              onChange={onPasswordChange(setPasswordParameters, setPassword)}
              className={styles.input}
            />
          </Form.Item>
          <Form.Item>
            <div className={styles.oauthSitesContainer}>
              <Tooltip className={`${styles.verticalGroup} ${getCircleStyle(0)}`} title="Minimum len is 10">
                <CheckCircleFilled />
                <span className={styles.circleCheckTitle}>10</span>
              </Tooltip>
              <Tooltip className={`${styles.verticalGroup} ${getCircleStyle(1)}`} title="Must contain at least one upper case">
                <CheckCircleFilled />
                <span className={styles.circleCheckTitle}>A</span>
              </Tooltip>
              <Tooltip className={`${styles.verticalGroup} ${getCircleStyle(2)}`} title="Must contain at least one lower case">
                <CheckCircleFilled />
                <span className={styles.circleCheckTitle}>a</span>
              </Tooltip>
              <Tooltip className={`${styles.verticalGroup} ${getCircleStyle(3)}`} title="Must contain at least one number">
                <CheckCircleFilled />
                <span className={styles.circleCheckTitle}>0</span>
              </Tooltip>
              <Tooltip className={`${styles.verticalGroup} ${getCircleStyle(4)}`} title="Must contain at least one special">
                <CheckCircleFilled />
                <span className={styles.circleCheckTitle}>&</span>
              </Tooltip>
            </div>
          </Form.Item>
          <Form.Item
            name="password_repeat"
            rules={[{ required: true, message: '' }]}
            validateStatus={passwordRepeat as any}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password repeat"
              size="large"
              onChange={onPasswordRepeatChange(setPasswordRepeat, password)}
              className={styles.input}
            />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" size="large" loading={isLoading}>
              Sign up
            </Button>
            <Button type="link" htmlType="button" size="large" style={{ color: "#b44ed9" }}>
              <Link to={{ pathname: '/sign-in' }}>Already have an account?</Link>
            </Button>
          </Form.Item>
          <Divider plain><span className={styles.onTop}>or continue with</span></Divider>
          <Form.Item>
            <OAuth github={{ redirectUri: `${process.env.REACT_APP_GITHUB_REDIRECT_URI_BASE}/sign-up`, clientId: process.env.REACT_APP_GITHUB_CLIENT_ID || '' }} google={{ clientId: process.env.REACT_APP_GOOGLE_CLIENT_ID || '' }} setLoading={setLoading} />
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default RegisterPage;