import apiClient from '../../helpers/api';

export interface RegisterRequest {
  username: string;
  email: string;
  fullName: string;
  password: string;
}

export const register = (body: RegisterRequest) => apiClient.post({ endpoint: '/auth/sign-up', body });