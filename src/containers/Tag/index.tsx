import { Spin } from 'antd';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import Header from '../../components/Header';
import HorizontalQuestionCard from '../../components/HorizontalQuestionCard';
import { loadUserData, useUserStore } from '../../helpers/userStore';
import { Question } from '../../types/question';
import { emptyTagInfo, TagInfo } from '../../types/tag';
import VerticalList from '../Dashboard/VerticalList';
import { loadTag, loadTagQuestions } from './actions';

import styles from './styles.module.sass';

const TagPage: React.FC = ({ }) => {
  const params = useParams();

  const [tag, setTag] = useState<TagInfo>(emptyTagInfo());
  const [questions, setQuestions] = useState<Question[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [questionsLoading, setQuestionsLoading] = useState<boolean>(false);

  const user = useUserStore(state => state.user);
  const setUser = useUserStore(state => state.setUser);

  useEffect(() => {
    document.title = `${tag.name.substring(0, 10)} - DataAns`;
  }, [tag]);

  useEffect(() => {
    const id = params.id;

    if (id) {
      loadTag(id, setTag, setLoading);
      loadTagQuestions(id, setQuestions, setQuestionsLoading);
    }
  }, [params]);

  useEffect(() => {
    loadUserData().then(user => {
      setUser(user);
    }).catch(e => console.error(e));
  }, []);

  return (
    <div className={styles.tagPage}>
      <Header user={user} />
      <div className={styles.page}>
        <div className={styles.tag}>
          <div className={styles.itemsList}>
            <span className={styles.tagName}>{tag.name}</span>
            {loading ?
              <Spin />
            :
              <div>
                <span>Created at {tag.createdAt.toLocaleString()} by </span>
                <Link to={{ pathname: `/user/${tag.creator.id}` }} className={styles.authorName}>
                  <span>{tag.creator.fullName}</span>
                  <img src={tag.creator.avatarUrl} />
                </Link>
              </div>
            }
          </div>
          <span>Total questions: {tag.questionsAmount}</span>
        </div>
        {questionsLoading ?
          <Spin />
        : 
          <VerticalList items={questions.map(question => ({ question }))} Component={HorizontalQuestionCard} maxWidth="70%" />
        }
      </div>
    </div>
  );
};

export default TagPage;