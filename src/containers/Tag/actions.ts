import { message } from 'antd';
import { loadTagInfo, getTagQuestions } from '../../helpers/api/tag';
import { Question } from '../../types/question';
import { TagInfo } from '../../types/tag';

export const loadTag = async (id: string, setTag: (t: TagInfo) => void, setLoading: (l: boolean) => void) => {
  setLoading(true);
  try {
    setTag(await loadTagInfo(id));
  } catch (e) {
    message.error(e + '');
  } finally {
    setLoading(false);
  }
}

export const loadTagQuestions = async (id: string, setQuestions: (t: Question[]) => void, setLoading: (l: boolean) => void) => {
  setLoading(true);
  try {
    setQuestions(await getTagQuestions(id));
  } catch (e) {
    message.error(e + '');
  } finally {
    setLoading(false);
  }
}
