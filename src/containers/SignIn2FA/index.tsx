import { Button, Input, message } from 'antd';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { submitTotp } from '../../helpers/api/user';

import styles from './styles.module.sass';

const SignIn2FA: React.FC = ({ }) => {
  const navigate = useNavigate();
  const [isLoading, setLoading] = useState<boolean>(false);
  const [code, setCode] = useState<string>('');

  const onFinish = async () => {
    setLoading(true);
    try {
      await submitTotp({ code });
      navigate('/');
    } catch (e) {
      message.error(e + '');
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className={styles.page}>
      <div className={styles.header}>
        <span>Dataans</span>
      </div>
      <div className={styles.form}>
        <span className={styles.title}>Two-Factor Authentication</span>
        <Input value={code} onChange={event => setCode(event.target.value)} className={styles.input}/>
        <span className={styles.info}>Enter the code from the two-factor app on your mobile device. If you've lost your device, you may enter one of your recovery codes.</span>
        <Button onClick={onFinish} loading={isLoading} type="primary">
          <span>Verify code</span>
        </Button>
        <Link to={{ pathname: '/sign-in' }}>
          <span>Back to sign in form</span>
        </Link>
      </div>
    </div>
  );
};

export default SignIn2FA;