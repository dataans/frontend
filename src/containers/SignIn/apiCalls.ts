import { SignInCredentials, SignInResponse, User } from '../../types/user';
import apiClient from '../../helpers/api';
import { message } from 'antd';

export const signIn = async (body: SignInCredentials): Promise<SignInResponse> => {
  try {
    const response = await apiClient.post({ endpoint: '/auth/sign-in', body });
    const signInResponse = await response.json();
    return signInResponse;
  } catch (e) {
    message.error(e + '');
    return { needs2fa: false };
  }
}