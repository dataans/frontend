import React, { useEffect, useState } from 'react';
import { Form, Input, Button, Divider, Tooltip } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { Link, useNavigate } from 'react-router-dom';
import { SignInCredentials } from '../../types/user';
import { signIn } from './apiCalls';
import OAuth from '../../components/OAuth';

import styles from './style.module.sass';
import titles from '../../styles/titles.module.sass';

const LoginPage: React.FC = () => {
  const navigate = useNavigate();
  const [isLoading, setLoading] = useState<boolean>(false);

  const onFinish = async (values: SignInCredentials) => {
    setLoading(true);
    try {
      let signInResponse = await signIn(values);
      if (signInResponse.needs2fa) {
        navigate('/sign-in/2fa');
      } else {
        navigate('/');
      }
    } catch (e) {
      console.error(e);
    }
    setLoading(false);
  };

  useEffect(() => {
    document.title = 'Sign in - DataAns'
  }, []);

  return (
    <div className={styles.loginPage}>
      <div className={styles.loginForm}>
        <div className={styles.formInnerWrapper} />
        <Form name="login" onFinish={onFinish} >
          <Form.Item>
            <span className={titles.formTitle}>Sign in</span>
          </Form.Item>
          <Form.Item
            name="login"
            rules={[{ required: true, message: 'Please input your Username or Email!' }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Username or Email"
              size="large"
              allowClear
              className={styles.input}
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: 'Please input your Password!' }]}
          >
            <div className={styles.passwordGroup}>
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Password"
                size="large"
                allowClear
                className={styles.input}
              />
              <Tooltip title="Forgot password">
                <Button type="primary" htmlType="button" size="large">
                  Forgot?
                </Button>
              </Tooltip>
            </div>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" size="large" loading={isLoading}>
              Sign in
            </Button>
            <Button type="link" htmlType="button" size="large">
              <Link to={{ pathname: '/sign-up' }}>Need an account?</Link>
            </Button>
          </Form.Item>
          <Divider plain><span className={styles.onTop}>or continue with</span></Divider>
          <Form.Item>
            <OAuth github={{ redirectUri: `${process.env.REACT_APP_GITHUB_REDIRECT_URI_BASE}/sign-in`, clientId: process.env.REACT_APP_GITHUB_CLIENT_ID || '' }} google={{ clientId: process.env.REACT_APP_GOOGLE_CLIENT_ID || '' }} setLoading={setLoading} />
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default LoginPage;