import React from 'react';
import { Routes, BrowserRouter, Route } from 'react-router-dom';

import LoginPage from '../SignIn';
import NotFound from '../NotFound';
import RegisterPage from '../SignUp';
import Dashboard from '../Dashboard';
import QuestionPage from '../Question';
import Profile from '../Profile';
import SignIn2FA from '../SignIn2FA';
import TagPage from '../Tag';

const Routing: React.FC = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/user/account" element={<Profile tab="account" />} />
        <Route path="/user/insights" element={<Profile tab="insights" />} />
        <Route path="/user/security" element={<Profile tab="security" />} />
        <Route path="/user/danger" element={<Profile tab="danger" />} />
        <Route path="/sign-in" element={<LoginPage />} />
        <Route path="/sign-in/2fa" element={<SignIn2FA />} />
        <Route path="/sign-up" element={<RegisterPage />} />
        <Route path="/" element={<Dashboard />} />
        <Route path="/question/:id" element={<QuestionPage />} />
        <Route path="/tag/:id" element={<TagPage />} />
        <Route path="/*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Routing;
