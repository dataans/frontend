import React from 'react';

const NotFound: React.FC = () => {
  return (
    <div>
      Error: Not found
    </div>
  );
};

export default NotFound;
